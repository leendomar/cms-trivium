<?php

namespace Trivium\Shortcode\Events;

use Trivium\Base\Events\Event;
use Illuminate\Foundation\Events\Dispatchable;

class ShortcodeAdminConfigRendering extends Event
{
    use Dispatchable;
}
