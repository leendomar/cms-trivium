<?php

return [
    'name' => 'Temas',
    'theme' => 'Tema',
    'author' => 'Autor',
    'version' => 'Versão',
    'active_success' => 'Ative o tema :name com sucesso!',
    'active' => 'Ativo',
    'activated' => 'Ativado',
    'appearance' => 'Aparência',
    'theme_options' => 'Opções do tema',
    'save_changes' => 'Salvar alterações',
    'custom_css' => 'CSS personalizado',
    'custom_js' => 'JS personalizado',
    'custom_header_js' => 'JS do cabeçalho',
    'custom_header_js_placeholder' => 'JS no cabeçalho da página, envolva-o dentro de &#x3C;script&#x3E;&#x3C;/script&#x3E;',
    'custom_body_js' => 'Body JS',
    'custom_body_js_placeholder' => 'JS no corpo da página, envolva-o dentro de &#x3C;script&#x3E;&#x3C;/script&#x3E;',
    'custom_footer_js' => 'Footer JS',
    'custom_footer_js_placeholder' => 'JS no rodapé da página, envolva-o dentro de &#x3C;script&#x3E;&#x3C;/script&#x3E;',
    'custom_html' => 'HTML Personalizado',
    'custom_header_html' => 'Header HTML',
    'custom_header_html_placeholder' => 'HTML no cabeçalho da página, sem tags especiais: script, style, iframe...',
    'custom_body_html' => 'Body HTML',
    'custom_body_html_placeholder' => 'HTML no corpo da página, sem tags especiais: script, estilo, iframe...',
    'custom_footer_html' => 'Footer HTML',
    'custom_footer_html_placeholder' => 'HTML no rodapé da página, sem tags especiais: script, estilo, iframe...',
    'remove_theme_success' => 'Tema removido com sucesso!',
    'theme_is_not_existed' => 'Este tema não existe!',
    'remove' => 'Remover',
    'remove_theme' => 'Remover tema',
    'remove_theme_confirm_message' => 'Você realmente quer remover este tema?',
    'remove_theme_confirm_yes' => 'Sim, remova-o!',
    'total_themes' => 'Total de temas',
    'add_new' => 'Adicionar',
    'theme_activated_already' => 'Tema ":name" já está ativado!',
    'theme_inherit_not_found' => 'O tema herdado ":name" não foi encontrado!',
    'missing_json_file' => 'Arquivo theme.json ausente!',
    'theme_invalid' => 'O tema é válido!',
    'published_assets_success' => 'Publicar ativos para :themes com sucesso!',
    'cannot_remove_theme' => 'Não é possível remover o tema ativado, ative outro tema antes de remover ":name"!',
    'cannot_remove_inherit_theme' => 'Não é possível remover o tema ":name" porque ele é herdado pelo tema atual!',
    'theme_deleted' => 'O tema ":name" foi destruído.',
    'removed_assets' => 'Remover ativos de um tema :name com sucesso!',
    'go_to_dashboard' => 'Ir para o painel',
    'theme_option_general' => 'Geral',
    'theme_option_general_description' => 'Configurações gerais',
    'theme_option_seo_open_graph_image' => 'Imagem padrão do Open Graph para SEO',
    'theme_option_logo' => 'Logotipo',
    'theme_option_favicon' => 'Favicon',
    'folder_is_not_writeable' => 'Não é possível gravar arquivos! A pasta :name não é gravável. Use chmod para torná-la gravável!',
    'breadcrumb_enabled' => 'Habilitar breadcrumb?',
    'themes' => 'Temas',
    'child_of' => 'Filho de ":theme"',
];
