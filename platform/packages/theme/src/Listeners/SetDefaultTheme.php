<?php

namespace Trivium\Theme\Listeners;

use Trivium\Setting\Facades\Setting;
use Trivium\Theme\Facades\Theme;

class SetDefaultTheme
{
    public function handle(): void
    {
        Setting::forceSet('theme', Theme::getThemeName())->set('show_admin_bar', 1)->save();
    }
}
