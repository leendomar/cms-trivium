<?php

namespace Trivium\Theme\Listeners;

use Trivium\Base\Events\FormRendering;
use Trivium\Base\Facades\AdminHelper;
use Trivium\JsValidation\Facades\JsValidator;
use Trivium\Theme\Facades\Theme;

class AddFormJsValidation
{
    public function handle(FormRendering $event): void
    {
        if (AdminHelper::isInAdmin()) {
            return;
        }

        $form = $event->form;

        if (! $form->getValidatorClass()) {
            return;
        }

        Theme::asset()
            ->container('footer')
            ->usePath(false)
            ->add('js-validation', 'vendor/core/core/js-validation/js/js-validation.js', ['jquery']);

        Theme::asset()
            ->container('footer')
            ->writeContent(
                'js-validation-form-rules',
                JsValidator::formRequest($form->getValidatorClass(), '#' . $form->getFormOption('id'))->render(),
                ['jquery']
            );
    }
}
