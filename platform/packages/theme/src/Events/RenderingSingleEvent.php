<?php

namespace Trivium\Theme\Events;

use Trivium\Base\Events\Event;
use Trivium\Slug\Models\Slug;
use Illuminate\Queue\SerializesModels;

class RenderingSingleEvent extends Event
{
    use SerializesModels;

    public function __construct(public Slug $slug)
    {
    }
}
