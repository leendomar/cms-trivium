<?php

namespace Trivium\Theme\Events;

use Trivium\Base\Events\Event;
use Illuminate\Foundation\Events\Dispatchable;

class RenderingAdminBar extends Event
{
    use Dispatchable;
}
