<?php

namespace Trivium\Theme\Events;

use Trivium\Base\Events\Event;
use Illuminate\Foundation\Events\Dispatchable;

class RenderingThemeOptionSettings extends Event
{
    use Dispatchable;
}
