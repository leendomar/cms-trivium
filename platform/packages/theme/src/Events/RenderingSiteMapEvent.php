<?php

namespace Trivium\Theme\Events;

use Trivium\Base\Events\Event;

class RenderingSiteMapEvent extends Event
{
    public function __construct(public string|null $key = null)
    {
    }
}
