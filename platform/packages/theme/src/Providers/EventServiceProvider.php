<?php

namespace Trivium\Theme\Providers;

use Trivium\Base\Events\FormRendering;
use Trivium\Base\Events\SeederPrepared;
use Trivium\Base\Events\SystemUpdateDBMigrated;
use Trivium\Base\Events\SystemUpdatePublished;
use Trivium\Theme\Listeners\AddFormJsValidation;
use Trivium\Theme\Listeners\CoreUpdateThemeDB;
use Trivium\Theme\Listeners\PublishThemeAssets;
use Trivium\Theme\Listeners\SetDefaultTheme;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        SystemUpdateDBMigrated::class => [
            CoreUpdateThemeDB::class,
        ],
        SystemUpdatePublished::class => [
            PublishThemeAssets::class,
        ],
        SeederPrepared::class => [
            SetDefaultTheme::class,
        ],
        FormRendering::class => [
            AddFormJsValidation::class,
        ],
    ];
}
