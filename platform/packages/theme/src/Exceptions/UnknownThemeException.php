<?php

namespace Trivium\Theme\Exceptions;

use UnexpectedValueException;

class UnknownThemeException extends UnexpectedValueException
{
}
