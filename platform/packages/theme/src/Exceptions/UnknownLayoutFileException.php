<?php

namespace Trivium\Theme\Exceptions;

use UnexpectedValueException;

class UnknownLayoutFileException extends UnexpectedValueException
{
}
