<?php

namespace Trivium\Theme\Exceptions;

use UnexpectedValueException;

class UnknownPartialFileException extends UnexpectedValueException
{
}
