<?php

namespace Trivium\Theme\Exceptions;

use UnexpectedValueException;

class UnknownViewFileException extends UnexpectedValueException
{
}
