<?php

namespace Trivium\Theme\Forms;

use Trivium\Base\Facades\BaseHelper;
use Trivium\Base\Forms\FieldOptions\CodeEditorFieldOption;
use Trivium\Base\Forms\Fields\CodeEditorField;
use Trivium\Base\Forms\FormAbstract;
use Trivium\Theme\Facades\Theme;
use Trivium\Theme\Http\Requests\CustomCssRequest;
use Illuminate\Support\Facades\File;

class CustomCSSForm extends FormAbstract
{
    public function setup(): void
    {
        $css = null;
        $file = Theme::getStyleIntegrationPath();

        if (File::exists($file)) {
            $css = BaseHelper::getFileData($file, false);
        }

        $this
            ->setUrl(route('theme.custom-css.post'))
            ->setValidatorClass(CustomCssRequest::class)
            ->setActionButtons(view('core/base::forms.partials.form-actions', ['onlySave' => true])->render())
            ->add(
                'custom_css',
                CodeEditorField::class,
                CodeEditorFieldOption::make()
                    ->label(trans('packages/theme::theme.custom_css'))
                    ->value($css)
                    ->mode('css')
                    ->toArray()
            );
    }
}
