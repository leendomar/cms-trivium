<?php

return [
    'locations' => [
        'main-menu' =>  __('Main Navigation'),
    ],
    'cache' => [
        'enabled' => env('CACHE_FRONTEND_MENU', false),
    ],
];
