<?php

namespace Trivium\Menu\Tables;

use Trivium\Menu\Models\Menu;
use Trivium\Table\Abstracts\TableAbstract;
use Trivium\Table\Actions\DeleteAction;
use Trivium\Table\Actions\EditAction;
use Trivium\Table\BulkActions\DeleteBulkAction;
use Trivium\Table\BulkChanges\CreatedAtBulkChange;
use Trivium\Table\BulkChanges\NameBulkChange;
use Trivium\Table\BulkChanges\StatusBulkChange;
use Trivium\Table\Columns\CreatedAtColumn;
use Trivium\Table\Columns\IdColumn;
use Trivium\Table\Columns\NameColumn;
use Trivium\Table\Columns\StatusColumn;
use Trivium\Table\HeaderActions\CreateHeaderAction;
use Illuminate\Database\Eloquent\Builder;

class MenuTable extends TableAbstract
{
    public function setup(): void
    {
        $this
            ->model(Menu::class)
            ->addColumns([
                IdColumn::make(),
                NameColumn::make()->route('menus.edit'),
                CreatedAtColumn::make(),
                StatusColumn::make(),
            ])
            ->addHeaderAction(CreateHeaderAction::make()->route('menus.create'))
            ->addActions([
                EditAction::make()->route('menus.edit'),
                DeleteAction::make()->route('menus.destroy'),
            ])
            ->addBulkAction(DeleteBulkAction::make()->permission('menus.destroy'))
            ->addBulkChanges([
                NameBulkChange::make(),
                StatusBulkChange::make(),
                CreatedAtBulkChange::make(),
            ])
            ->queryUsing(function (Builder $query) {
                $query
                    ->select([
                        'id',
                        'name',
                        'created_at',
                        'status',
                    ]);
            });
    }
}
