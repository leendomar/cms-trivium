<?php

namespace Trivium\Menu\Models;

use Trivium\Base\Casts\SafeContent;
use Trivium\Base\Enums\BaseStatusEnum;
use Trivium\Base\Models\BaseModel;
use Trivium\Base\Models\Concerns\HasSlug;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Menu extends BaseModel
{
    use HasSlug;

    protected $table = 'menus';

    protected $fillable = [
        'name',
        'slug',
        'status',
    ];

    protected $casts = [
        'status' => BaseStatusEnum::class,
        'name' => SafeContent::class,
    ];

    protected static function booted(): void
    {
        static::deleted(function (self $model) {
            $model->menuNodes()->delete();
            $model->locations()->delete();
        });

        self::saving(function (self $model) {
            $model->slug = self::createSlug($model->name, $model->getKey());
        });
    }

    public function menuNodes(): HasMany
    {
        return $this->hasMany(MenuNode::class, 'menu_id');
    }

    public function locations(): HasMany
    {
        return $this->hasMany(MenuLocation::class, 'menu_id');
    }
}
