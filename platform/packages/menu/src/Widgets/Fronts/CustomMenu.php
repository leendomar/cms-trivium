<?php

namespace Trivium\Menu\Widgets\Fronts;

use Trivium\Base\Forms\FieldOptions\NameFieldOption;
use Trivium\Base\Forms\FieldOptions\SelectFieldOption;
use Trivium\Base\Forms\Fields\SelectField;
use Trivium\Base\Forms\Fields\TextField;
use Trivium\Menu\Models\Menu;
use Trivium\Widget\AbstractWidget;
use Trivium\Widget\Forms\WidgetForm;

class CustomMenu extends AbstractWidget
{
    public function __construct()
    {
        parent::__construct([
            'name' => trans('packages/widget::widget.widget_custom_menu'),
            'description' => trans('packages/widget::widget.widget_custom_menu_description'),
            'menu_id' => null,
        ]);
    }

    protected function settingForm(): WidgetForm|string|null
    {
        return WidgetForm::createFromArray($this->getConfig())
            ->add('name', TextField::class, NameFieldOption::make()->toArray())
            ->add(
                'menu_id',
                SelectField::class,
                SelectFieldOption::make()
                    ->label(__('Menu'))
                    ->choices(Menu::query()->pluck('name', 'slug')->all())
                    ->searchable()
                    ->toArray()
            );
    }
}
