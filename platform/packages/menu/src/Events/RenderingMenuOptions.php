<?php

namespace Trivium\Menu\Events;

use Trivium\Base\Events\Event;
use Illuminate\Foundation\Events\Dispatchable;

class RenderingMenuOptions extends Event
{
    use Dispatchable;
}
