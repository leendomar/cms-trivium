<?php

namespace Trivium\Menu\Listeners;

use Trivium\Base\Contracts\BaseModel;
use Trivium\Base\Events\DeletedContentEvent;
use Trivium\Menu\Facades\Menu;
use Trivium\Menu\Models\MenuNode;

class DeleteMenuNodeListener
{
    public function handle(DeletedContentEvent $event): void
    {
        if (
            ! $event->data instanceof BaseModel ||
            ! in_array($event->data::class, Menu::getMenuOptionModels())
        ) {
            return;
        }

        MenuNode::query()
            ->where([
                'reference_id' => $event->data->getKey(),
                'reference_type' => $event->data::class,
            ])
            ->delete();
    }
}
