<?php

namespace Trivium\Menu\Repositories\Caches;

use Trivium\Menu\Repositories\Eloquent\MenuNodeRepository;

/**
 * @deprecated
 */
class MenuNodeCacheDecorator extends MenuNodeRepository
{
}
