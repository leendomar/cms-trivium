<?php

namespace Trivium\Menu\Repositories\Caches;

use Trivium\Menu\Repositories\Eloquent\MenuLocationRepository;

/**
 * @deprecated
 */
class MenuLocationCacheDecorator extends MenuLocationRepository
{
}
