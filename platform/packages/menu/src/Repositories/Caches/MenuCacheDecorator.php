<?php

namespace Trivium\Menu\Repositories\Caches;

use Trivium\Menu\Repositories\Eloquent\MenuRepository;

/**
 * @deprecated
 */
class MenuCacheDecorator extends MenuRepository
{
}
