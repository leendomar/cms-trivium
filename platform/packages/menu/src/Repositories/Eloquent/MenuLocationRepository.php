<?php

namespace Trivium\Menu\Repositories\Eloquent;

use Trivium\Menu\Repositories\Interfaces\MenuLocationInterface;
use Trivium\Support\Repositories\Eloquent\RepositoriesAbstract;

class MenuLocationRepository extends RepositoriesAbstract implements MenuLocationInterface
{
}
