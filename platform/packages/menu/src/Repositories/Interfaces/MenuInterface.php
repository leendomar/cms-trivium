<?php

namespace Trivium\Menu\Repositories\Interfaces;

use Trivium\Base\Models\BaseModel;
use Trivium\Support\Repositories\Interfaces\RepositoryInterface;

interface MenuInterface extends RepositoryInterface
{
    public function findBySlug(string $slug, bool $active, array $select = [], array $with = []): BaseModel|null;

    public function createSlug(string $name): string;
}
