<?php

namespace Trivium\Menu\Repositories\Interfaces;

use Trivium\Support\Repositories\Interfaces\RepositoryInterface;

interface MenuLocationInterface extends RepositoryInterface
{
}
