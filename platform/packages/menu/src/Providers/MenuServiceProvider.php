<?php

namespace Trivium\Menu\Providers;

use Trivium\Base\Facades\DashboardMenu;
use Trivium\Base\Supports\ServiceProvider;
use Trivium\Base\Traits\LoadAndPublishDataTrait;
use Trivium\Menu\Models\Menu as MenuModel;
use Trivium\Menu\Models\MenuLocation;
use Trivium\Menu\Models\MenuNode;
use Trivium\Menu\Repositories\Eloquent\MenuLocationRepository;
use Trivium\Menu\Repositories\Eloquent\MenuNodeRepository;
use Trivium\Menu\Repositories\Eloquent\MenuRepository;
use Trivium\Menu\Repositories\Interfaces\MenuInterface;
use Trivium\Menu\Repositories\Interfaces\MenuLocationInterface;
use Trivium\Menu\Repositories\Interfaces\MenuNodeInterface;
use Trivium\Theme\Events\RenderingAdminBar;
use Trivium\Theme\Facades\AdminBar;

class MenuServiceProvider extends ServiceProvider
{
    use LoadAndPublishDataTrait;

    public function register(): void
    {
        $this->app->bind(MenuInterface::class, function () {
            return new MenuRepository(new MenuModel());
        });

        $this->app->bind(MenuNodeInterface::class, function () {
            return new MenuNodeRepository(new MenuNode());
        });

        $this->app->bind(MenuLocationInterface::class, function () {
            return new MenuLocationRepository(new MenuLocation());
        });
    }

    public function boot(): void
    {
        $this
            ->setNamespace('packages/menu')
            ->loadAndPublishConfigurations(['permissions', 'general'])
            ->loadHelpers()
            ->loadRoutes()
            ->loadAndPublishViews()
            ->loadAndPublishTranslations()
            ->loadMigrations()
            ->publishAssets();

        DashboardMenu::default()->beforeRetrieving(function () {
            DashboardMenu::make()
                ->registerItem([
                    'id' => 'cms-core-menu',
                    'priority' => 2,
                    'parent_id' => 'cms-core-appearance',
                    'name' => 'packages/menu::menu.name',
                    'route' => 'menus.index',
                ]);
        });

        $this->app['events']->listen(RenderingAdminBar::class, function () {
            AdminBar::registerLink(
                trans('packages/menu::menu.name'),
                route('menus.index'),
                'appearance',
                'menus.index'
            );
        });

        $this->app->register(EventServiceProvider::class);
        $this->app->register(CommandServiceProvider::class);
    }
}
