<?php

namespace Trivium\Menu\Providers;

use Trivium\Base\Events\DeletedContentEvent;
use Trivium\Menu\Listeners\DeleteMenuNodeListener;
use Trivium\Menu\Listeners\UpdateMenuNodeUrlListener;
use Trivium\Slug\Events\UpdatedSlugEvent;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        UpdatedSlugEvent::class => [
            UpdateMenuNodeUrlListener::class,
        ],
        DeletedContentEvent::class => [
            DeleteMenuNodeListener::class,
        ],
    ];
}
