<?php

return [
    'permalink_settings' => 'Link permanente (SLUG)',
    'permalink_settings_description' => 'Visualizar e atualizar suas configurações de link permanente',
    'settings' => [
        'title' => 'Configurações de link permanente',
        'description' => 'Gerenciar link permanente para todos os módulos.',
        'preview' => 'Visualizar',
        'helper_text' => 'Visualizar: :url',
        'turn_off_automatic_url_translation_into_latin' => 'Desativar a tradução automática de URL para o latim?',
        'available_variables' => 'Variáveis ​​disponíveis',
    ],
    'preview' => 'Visualizar',
    'prefix_for' => 'Prefixo para :name',
    'public_single_ending_url' => 'Postfix para URL de página única',
    'current_year' => 'Ano atual',
    'current_month' => 'Mês atual',
    'current_day' => 'Dia atual',
    'generate_url' => 'Gerar URL',
];
