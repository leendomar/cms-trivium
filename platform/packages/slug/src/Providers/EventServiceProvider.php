<?php

namespace Trivium\Slug\Providers;

use Trivium\Base\Events\CreatedContentEvent;
use Trivium\Base\Events\DeletedContentEvent;
use Trivium\Base\Events\FinishedSeederEvent;
use Trivium\Base\Events\SeederPrepared;
use Trivium\Base\Events\UpdatedContentEvent;
use Trivium\Slug\Listeners\CreatedContentListener;
use Trivium\Slug\Listeners\CreateMissingSlug;
use Trivium\Slug\Listeners\DeletedContentListener;
use Trivium\Slug\Listeners\TruncateSlug;
use Trivium\Slug\Listeners\UpdatedContentListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        UpdatedContentEvent::class => [
            UpdatedContentListener::class,
        ],
        CreatedContentEvent::class => [
            CreatedContentListener::class,
        ],
        DeletedContentEvent::class => [
            DeletedContentListener::class,
        ],
        SeederPrepared::class => [
            TruncateSlug::class,
        ],
        FinishedSeederEvent::class => [
            CreateMissingSlug::class,
        ],
    ];
}
