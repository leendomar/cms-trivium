<?php

namespace Trivium\Slug\Forms\Fields;

use Trivium\Base\Forms\FormField;

class PermalinkField extends FormField
{
    protected function getTemplate(): string
    {
        return 'packages/slug::forms.fields.permalink';
    }
}
