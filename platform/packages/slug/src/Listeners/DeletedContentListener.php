<?php

namespace Trivium\Slug\Listeners;

use Trivium\Base\Contracts\BaseModel;
use Trivium\Base\Events\DeletedContentEvent;
use Trivium\Slug\Facades\SlugHelper;
use Trivium\Slug\Models\Slug;

class DeletedContentListener
{
    public function handle(DeletedContentEvent $event): void
    {
        if ($event->data instanceof BaseModel && SlugHelper::isSupportedModel($event->data::class)) {
            Slug::query()->where([
                'reference_id' => $event->data->getKey(),
                'reference_type' => $event->data::class,
            ])->delete();
        }
    }
}
