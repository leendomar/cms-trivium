<?php

namespace Trivium\Slug\Listeners;

use Trivium\Slug\Models\Slug;

class TruncateSlug
{
    public function handle(): void
    {
        Slug::query()->truncate();
    }
}
