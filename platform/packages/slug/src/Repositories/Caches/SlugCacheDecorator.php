<?php

namespace Trivium\Slug\Repositories\Caches;

use Trivium\Slug\Repositories\Eloquent\SlugRepository;

/**
 * @deprecated
 */
class SlugCacheDecorator extends SlugRepository
{
}
