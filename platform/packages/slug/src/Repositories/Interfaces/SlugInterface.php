<?php

namespace Trivium\Slug\Repositories\Interfaces;

use Trivium\Support\Repositories\Interfaces\RepositoryInterface;

interface SlugInterface extends RepositoryInterface
{
}
