<?php

namespace Trivium\Slug\Repositories\Eloquent;

use Trivium\Slug\Repositories\Interfaces\SlugInterface;
use Trivium\Support\Repositories\Eloquent\RepositoriesAbstract;

class SlugRepository extends RepositoriesAbstract implements SlugInterface
{
}
