<?php

namespace Trivium\Widget\Repositories\Caches;

use Trivium\Widget\Repositories\Eloquent\WidgetRepository;

/**
 * @deprecated
 */
class WidgetCacheDecorator extends WidgetRepository
{
}
