<?php

namespace Trivium\Widget\Events;

use Trivium\Base\Events\Event;
use Illuminate\Foundation\Events\Dispatchable;

class RenderingWidgetSettings extends Event
{
    use Dispatchable;
}
