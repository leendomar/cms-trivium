<?php

namespace Trivium\Widget\Http\Requests;

use Trivium\Support\Http\Requests\Request;

class WidgetRequest extends Request
{
    public function rules(): array
    {
        return [];
    }
}
