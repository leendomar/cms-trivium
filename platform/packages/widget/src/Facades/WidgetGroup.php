<?php

namespace Trivium\Widget\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static \Trivium\Widget\WidgetGroup group(string $sidebarId)
 * @method static \Trivium\Widget\WidgetGroupCollection setGroup(array $args)
 * @method static \Trivium\Widget\WidgetGroupCollection removeGroup(string $groupId)
 * @method static array getGroups()
 * @method static string render(string $sidebarId)
 * @method static void load(bool $force = false)
 * @method static \Illuminate\Support\Collection getData()
 *
 * @see \Trivium\Widget\WidgetGroupCollection
 */
class WidgetGroup extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'trivium.widget-group-collection';
    }
}
