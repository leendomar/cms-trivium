<?php

namespace Trivium\Widget\Facades;

use Trivium\Widget\WidgetGroup;
use Illuminate\Support\Facades\Facade;

/**
 * @method static \Trivium\Widget\Factories\WidgetFactory registerWidget(string $widget)
 * @method static array getWidgets()
 * @method static \Illuminate\Support\HtmlString|string|null run()
 *
 * @see \Trivium\Widget\Factories\WidgetFactory
 */
class Widget extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'trivium.widget';
    }

    public static function group(string $name): WidgetGroup
    {
        return app('trivium.widget-group-collection')->group($name);
    }
}
