<?php

namespace Trivium\Optimize\Http\Controllers\Settings;

use Trivium\Base\Http\Responses\BaseHttpResponse;
use Trivium\Optimize\Forms\Settings\OptimizeSettingForm;
use Trivium\Optimize\Http\Requests\OptimizeSettingRequest;
use Trivium\Setting\Http\Controllers\SettingController;

class OptimizeSettingController extends SettingController
{
    public function edit()
    {
        $this->pageTitle(trans('packages/optimize::optimize.settings.title'));

        return OptimizeSettingForm::create()->renderForm();
    }

    public function update(OptimizeSettingRequest $request): BaseHttpResponse
    {
        return $this->performUpdate($request->validated());
    }
}
