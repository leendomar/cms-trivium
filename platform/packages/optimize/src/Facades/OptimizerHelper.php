<?php

namespace Trivium\Optimize\Facades;

use Trivium\Optimize\Supports\Optimizer;
use Illuminate\Support\Facades\Facade;

/**
 * @method static bool isEnabled()
 * @method static \Trivium\Optimize\Supports\Optimizer enable()
 * @method static \Trivium\Optimize\Supports\Optimizer disable()
 *
 * @see \Trivium\Optimize\Supports\Optimizer
 */
class OptimizerHelper extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return Optimizer::class;
    }
}
