<?php

namespace Trivium\Page\Repositories\Caches;

use Trivium\Page\Repositories\Eloquent\PageRepository;

/**
 * @deprecated
 */
class PageCacheDecorator extends PageRepository
{
}
