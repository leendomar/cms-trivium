<?php

namespace Trivium\Page\Http\Controllers;

use Trivium\Base\Http\Controllers\BaseController;
use Trivium\Page\Models\Page;
use Trivium\Page\Services\PageService;
use Trivium\Slug\Facades\SlugHelper;
use Trivium\Theme\Events\RenderingSingleEvent;
use Trivium\Theme\Facades\Theme;

class PublicController extends BaseController
{
    public function getPage(string $slug, PageService $pageService)
    {
        $slug = SlugHelper::getSlug($slug, SlugHelper::getPrefix(Page::class));

        if (! $slug) {
            abort(404);
        }

        $data = $pageService->handleFrontRoutes($slug);

        if (isset($data['slug']) && $data['slug'] !== $slug->key) {
            return redirect()->to(url(SlugHelper::getPrefix(Page::class) . '/' . $data['slug']));
        }

        event(new RenderingSingleEvent($slug));

        return Theme::scope($data['view'], $data['data'], $data['default_view'])->render();
    }
}
