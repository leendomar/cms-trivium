<?php

namespace Trivium\Page\Forms;

use Trivium\Base\Forms\FieldOptions\ContentFieldOption;
use Trivium\Base\Forms\FieldOptions\DescriptionFieldOption;
use Trivium\Base\Forms\FieldOptions\NameFieldOption;
use Trivium\Base\Forms\FieldOptions\SelectFieldOption;
use Trivium\Base\Forms\FieldOptions\StatusFieldOption;
use Trivium\Base\Forms\Fields\EditorField;
use Trivium\Base\Forms\Fields\MediaImageField;
use Trivium\Base\Forms\Fields\SelectField;
use Trivium\Base\Forms\Fields\TextareaField;
use Trivium\Base\Forms\Fields\TextField;
use Trivium\Base\Forms\FormAbstract;
use Trivium\Page\Http\Requests\PageRequest;
use Trivium\Page\Models\Page;
use Trivium\Page\Supports\Template;

class PageForm extends FormAbstract
{
    public function setup(): void
    {
        $this
            ->model(Page::class)
            ->setValidatorClass(PageRequest::class)
            ->hasTabs()
            ->add('name', TextField::class, NameFieldOption::make()->maxLength(120)->required()->toArray())
            ->add('description', TextareaField::class, DescriptionFieldOption::make()->toArray())
            ->add('content', EditorField::class, ContentFieldOption::make()->allowedShortcodes()->toArray())
            ->add('status', SelectField::class, StatusFieldOption::make()->toArray())
            ->when(Template::getPageTemplates(), function (PageForm $form, array $templates) {
                return $form
                    ->add(
                        'template',
                        SelectField::class,
                        SelectFieldOption::make()
                            ->label(trans('core/base::forms.template'))
                            ->required()
                            ->choices($templates)
                            ->toArray()
                    );
            })
            ->add('image', MediaImageField::class)
            ->setBreakFieldPoint('status');
    }
}
