<?php

return [
    'create' => 'Criar Página',
    'form' => [
        'name' => 'Nome',
        'name_placeholder' => 'Nome da página (máximo de 120 caracteres)',
        'content' => 'Conteúdo',
        'note' => 'Conteúdo da nota',
    ],
    'notices' => [
        'no_select' => 'Selecione pelo menos um registro para executar esta ação!',
        'update_success_message' => 'Atualização bem-sucedida',
    ],
    'cannot_delete' => 'A página não pôde ser excluída',
    'deleted' => 'Página Excluída',
    'pages' => 'Páginas',
    'menu' => 'Páginas',
    'menu_name' => 'Páginas',
    'edit_this_page' => 'Editar Página',
    'total_pages' => 'Total de páginas',
    'front_page' => 'Página Inicial',
    'theme_options' => [
        'title' => 'Página',
        'your_home_page_display' => 'Página inicial',
    ],
];
