<?php

return [
    'meta_box_header' => 'Otimização de mecanismos de busca',
    'edit_seo_meta' => 'Editar meta de SEO',
    'default_description' => 'Configurar meta título e descrição para tornar seu site fácil de ser descoberto em mecanismos de busca como o Google',
    'seo_title' => 'Título de SEO',
    'seo_description' => 'Descrição de SEO',
    'allow_index' => 'Permitir que mecanismos de busca indexem esta página',
    'index' => 'Índice',
    'noindex' => 'Sem índice',
];
