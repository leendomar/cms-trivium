<?php

namespace Trivium\SeoHelper\Providers;

use Trivium\Base\Events\CreatedContentEvent;
use Trivium\Base\Events\DeletedContentEvent;
use Trivium\Base\Events\UpdatedContentEvent;
use Trivium\SeoHelper\Listeners\CreatedContentListener;
use Trivium\SeoHelper\Listeners\DeletedContentListener;
use Trivium\SeoHelper\Listeners\UpdatedContentListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        UpdatedContentEvent::class => [
            UpdatedContentListener::class,
        ],
        CreatedContentEvent::class => [
            CreatedContentListener::class,
        ],
        DeletedContentEvent::class => [
            DeletedContentListener::class,
        ],
    ];
}
