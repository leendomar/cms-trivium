<?php

namespace Trivium\SeoHelper\Providers;

use Trivium\Base\Supports\ServiceProvider;
use Trivium\Base\Traits\LoadAndPublishDataTrait;
use Trivium\SeoHelper\Contracts\SeoHelperContract;
use Trivium\SeoHelper\Contracts\SeoMetaContract;
use Trivium\SeoHelper\Contracts\SeoOpenGraphContract;
use Trivium\SeoHelper\Contracts\SeoTwitterContract;
use Trivium\SeoHelper\SeoHelper;
use Trivium\SeoHelper\SeoMeta;
use Trivium\SeoHelper\SeoOpenGraph;
use Trivium\SeoHelper\SeoTwitter;

/**
 * @since 02/12/2015 14:09 PM
 */
class SeoHelperServiceProvider extends ServiceProvider
{
    use LoadAndPublishDataTrait;

    public function register(): void
    {
        $this->app->bind(SeoMetaContract::class, SeoMeta::class);
        $this->app->bind(SeoHelperContract::class, SeoHelper::class);
        $this->app->bind(SeoOpenGraphContract::class, SeoOpenGraph::class);
        $this->app->bind(SeoTwitterContract::class, SeoTwitter::class);
    }

    public function boot(): void
    {
        $this
            ->setNamespace('packages/seo-helper')
            ->loadHelpers()
            ->loadAndPublishConfigurations(['general'])
            ->loadAndPublishViews()
            ->loadAndPublishTranslations()
            ->publishAssets();

        $this->app->register(EventServiceProvider::class);
        $this->app->register(HookServiceProvider::class);
    }
}
