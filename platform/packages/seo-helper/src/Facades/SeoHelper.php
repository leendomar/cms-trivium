<?php

namespace Trivium\SeoHelper\Facades;

use Trivium\SeoHelper\SeoHelper as BaseSeoHelper;
use Illuminate\Support\Facades\Facade;

/**
 * @method static \Trivium\SeoHelper\SeoHelper setSeoMeta(\Trivium\SeoHelper\Contracts\SeoMetaContract $seoMeta)
 * @method static \Trivium\SeoHelper\SeoHelper setSeoOpenGraph(\Trivium\SeoHelper\Contracts\SeoOpenGraphContract $seoOpenGraph)
 * @method static \Trivium\SeoHelper\SeoHelper setSeoTwitter(\Trivium\SeoHelper\Contracts\SeoTwitterContract $seoTwitter)
 * @method static \Trivium\SeoHelper\Contracts\SeoOpenGraphContract openGraph()
 * @method static \Trivium\SeoHelper\SeoHelper setTitle(string|null $title, string|null $siteName = null, string|null $separator = null)
 * @method static \Trivium\SeoHelper\Contracts\SeoMetaContract meta()
 * @method static \Trivium\SeoHelper\Contracts\SeoTwitterContract twitter()
 * @method static string|null getTitle()
 * @method static string|null getDescription()
 * @method static \Trivium\SeoHelper\SeoHelper setDescription($description)
 * @method static mixed render()
 * @method static bool saveMetaData(string $screen, \Illuminate\Http\Request $request, \Illuminate\Database\Eloquent\Model $object)
 * @method static bool deleteMetaData(string $screen, \Illuminate\Database\Eloquent\Model $object)
 * @method static \Trivium\SeoHelper\SeoHelper registerModule(array|string $model)
 *
 * @see \Trivium\SeoHelper\SeoHelper
 */
class SeoHelper extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return BaseSeoHelper::class;
    }
}
