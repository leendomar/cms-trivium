<?php

namespace Trivium\SeoHelper\Exceptions;

class InvalidTwitterCardException extends InvalidArgumentException
{
}
