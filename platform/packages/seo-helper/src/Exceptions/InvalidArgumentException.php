<?php

namespace Trivium\SeoHelper\Exceptions;

class InvalidArgumentException extends SeoHelperException
{
}
