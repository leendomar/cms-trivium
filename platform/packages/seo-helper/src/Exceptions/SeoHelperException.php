<?php

namespace Trivium\SeoHelper\Exceptions;

use Exception;

abstract class SeoHelperException extends Exception
{
}
