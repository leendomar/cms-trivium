<?php

namespace Trivium\SeoHelper\Entities\OpenGraph;

use Trivium\SeoHelper\Bases\MetaCollection as BaseMetaCollection;

class MetaCollection extends BaseMetaCollection
{
    protected $prefix = 'og:';

    protected $nameProperty = 'property';
}
