<?php

namespace Trivium\SeoHelper\Entities\Twitter;

use Trivium\SeoHelper\Bases\MetaCollection as BaseMetaCollection;

class MetaCollection extends BaseMetaCollection
{
    /**
     * Meta tag prefix.
     *
     * @var string
     */
    protected $prefix = 'twitter:';
}
