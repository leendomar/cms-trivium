<?php

namespace Trivium\SeoHelper\Listeners;

use Trivium\Base\Events\CreatedContentEvent;
use Trivium\Base\Facades\BaseHelper;
use Trivium\SeoHelper\Facades\SeoHelper;
use Exception;

class CreatedContentListener
{
    public function handle(CreatedContentEvent $event): void
    {
        try {
            SeoHelper::saveMetaData($event->screen, $event->request, $event->data);
        } catch (Exception $exception) {
            BaseHelper::logError($exception);
        }
    }
}
