<?php

namespace Trivium\SeoHelper\Listeners;

use Trivium\Base\Events\UpdatedContentEvent;
use Trivium\Base\Facades\BaseHelper;
use Trivium\SeoHelper\Facades\SeoHelper;
use Exception;

class UpdatedContentListener
{
    public function handle(UpdatedContentEvent $event): void
    {
        try {
            SeoHelper::saveMetaData($event->screen, $event->request, $event->data);
        } catch (Exception $exception) {
            BaseHelper::logError($exception);
        }
    }
}
