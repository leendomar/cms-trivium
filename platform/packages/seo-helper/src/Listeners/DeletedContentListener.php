<?php

namespace Trivium\SeoHelper\Listeners;

use Trivium\Base\Events\DeletedContentEvent;
use Trivium\Base\Facades\BaseHelper;
use Trivium\SeoHelper\Facades\SeoHelper;
use Exception;

class DeletedContentListener
{
    public function handle(DeletedContentEvent $event): void
    {
        try {
            SeoHelper::deleteMetaData($event->screen, $event->data);
        } catch (Exception $exception) {
            BaseHelper::logError($exception);
        }
    }
}
