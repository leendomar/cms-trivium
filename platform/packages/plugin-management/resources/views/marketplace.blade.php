@extends(BaseHelper::getAdminMasterLayoutTemplate())

@section('content')
    <div role="alert" class="alert alert-warning">
        Esses plugins são da nossa comunidade Trivium <a href="https://marketplace.trivium.dev.br/products" target="_blank">marketplace.trivium.dev.br/products</a>. Lamentamos informar
    que não podemos assumir responsabilidade pela funcionalidade ou suporte de plugins gratuitos, pois eles são
    desenvolvidos e mantidos de forma independente. No entanto, estamos mais do que felizes em ajudar com quaisquer dúvidas ou
    problemas relacionados aos nossos produtos e serviços oficiais.
    </div>

    <plugin-list plugin-list-url="{{ route('plugins.marketplace.ajax.list') }}" plugin-remove-url="{{ route('plugins.remove', '__name__') }}"></plugins-list>
@endsection

@push('footer')
    <x-core::modal
        id="terms-and-policy-modal"
        :title="__('Install plugin from Marketplace')"
        :submit-button-label="__('Accept and install')"
        size="md"
    >
        <div class="text-start">
        <p>
        Você está instalando um plugin da nossa comunidade Trivium. Esses plugins são desenvolvidos pelo autor
        em <a href="https://marketplace.trivium.dev.br" target="_blank">marketplace.trivium.dev.br</a>.
        </p>
        <p>Nós (Trivium) <strong>não</strong> daremos suporte a plugins gratuitos do Marketplace.</p>
        <p>
        Se tiver problemas ou bugs, entre em contato com o autor desse plugin para obter suporte ou apenas
        exclua-o da página <a :href="installedPluginsUrl">Plugins instalados</a>.
        </p>
        <p class="mb-0">
        Se isso fizer seu site cair, apenas exclua esse plugin da
        pasta <code>platform/plugins</code>.
        </p>
        </div>

        <x-slot:footer>
            <div class="w-100">
                <div class="row">
                    <div class="col">
                        <button type="button" class="btn w-100" data-bs-dismiss="modal">
                            {{ __('Cancel') }}
                        </button>
                    </div>
                    <div class="col">
                        <button type="button" class="btn btn-info w-100" data-bb-toggle="accept-term-and-policy">
                            {{ __('Accept and install') }}
                        </button>
                    </div>
                </div>
            </div>
        </x-slot:footer>
    </x-core::modal>

    <script>
        window.trans = {{ Js::from([
            'base' => trans('packages/plugin-management::marketplace'),
        ]) }};

        window.marketplace = {
            route: {
                list: "{{ route('plugins.marketplace.ajax.list') }}",
                detail: "{{ route('plugins.marketplace.ajax.detail', [':id']) }}",
                install: "{{ route('plugins.marketplace.ajax.install', [':id']) }}",
                active: "{{ route('plugins.change.status') }}",
            },
            installed: {{ Js::from(get_installed_plugins()) }},
            activated: {{ Js::from(get_active_plugins()) }},
            token: "{{ csrf_token() }}",
            coreVersion: "{{ get_cms_version() }}"
        };
    </script>
@endpush
