<?php

namespace Trivium\PluginManagement\Events;

use Trivium\Base\Events\Event;
use Illuminate\Foundation\Events\Dispatchable;

class RenderingPluginListingPage extends Event
{
    use Dispatchable;
}
