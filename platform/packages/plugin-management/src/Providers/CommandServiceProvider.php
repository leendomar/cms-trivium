<?php

namespace Trivium\PluginManagement\Providers;

use Trivium\Base\Supports\ServiceProvider;
use Trivium\PluginManagement\Commands\ClearCompiledCommand;
use Trivium\PluginManagement\Commands\IlluminateClearCompiledCommand as OverrideIlluminateClearCompiledCommand;
use Trivium\PluginManagement\Commands\PackageDiscoverCommand;
use Trivium\PluginManagement\Commands\PluginActivateAllCommand;
use Trivium\PluginManagement\Commands\PluginActivateCommand;
use Trivium\PluginManagement\Commands\PluginAssetsPublishCommand;
use Trivium\PluginManagement\Commands\PluginDeactivateAllCommand;
use Trivium\PluginManagement\Commands\PluginDeactivateCommand;
use Trivium\PluginManagement\Commands\PluginDiscoverCommand;
use Trivium\PluginManagement\Commands\PluginInstallFromMarketplaceCommand;
use Trivium\PluginManagement\Commands\PluginListCommand;
use Trivium\PluginManagement\Commands\PluginRemoveAllCommand;
use Trivium\PluginManagement\Commands\PluginRemoveCommand;
use Illuminate\Foundation\Console\ClearCompiledCommand as IlluminateClearCompiledCommand;
use Illuminate\Foundation\Console\PackageDiscoverCommand as IlluminatePackageDiscoverCommand;

class CommandServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->extend(IlluminatePackageDiscoverCommand::class, function () {
            return $this->app->make(PackageDiscoverCommand::class);
        });

        $this->app->extend(IlluminateClearCompiledCommand::class, function () {
            return $this->app->make(OverrideIlluminateClearCompiledCommand::class);
        });
    }

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                PluginAssetsPublishCommand::class,
                ClearCompiledCommand::class,
                PluginDiscoverCommand::class,
                PluginInstallFromMarketplaceCommand::class,
                PluginActivateCommand::class,
                PluginActivateAllCommand::class,
                PluginDeactivateCommand::class,
                PluginDeactivateAllCommand::class,
                PluginRemoveCommand::class,
                PluginRemoveAllCommand::class,
                PluginListCommand::class,
            ]);
        }
    }
}
