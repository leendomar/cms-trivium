<?php

namespace Trivium\PluginManagement\Providers;

use Trivium\Base\Events\SeederPrepared;
use Trivium\Base\Events\SystemUpdateDBMigrated;
use Trivium\Base\Events\SystemUpdatePublished;
use Trivium\Base\Listeners\ClearDashboardMenuCaches;
use Trivium\PluginManagement\Events\ActivatedPluginEvent;
use Trivium\PluginManagement\Listeners\ActivateAllPlugins;
use Trivium\PluginManagement\Listeners\ClearPluginCaches;
use Trivium\PluginManagement\Listeners\CoreUpdatePluginsDB;
use Trivium\PluginManagement\Listeners\PublishPluginAssets;
use Illuminate\Contracts\Database\Events\MigrationEvent;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        MigrationEvent::class => [
            ClearPluginCaches::class,
        ],
        SystemUpdateDBMigrated::class => [
            CoreUpdatePluginsDB::class,
        ],
        SystemUpdatePublished::class => [
            PublishPluginAssets::class,
        ],
        SeederPrepared::class => [
            ActivateAllPlugins::class,
        ],
        ActivatedPluginEvent::class => [
            ClearDashboardMenuCaches::class,
        ],
    ];
}
