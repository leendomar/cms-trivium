<?php

use Trivium\Page\Models\Page;

return [
    'supported' => [
        Page::class,
    ],
];
