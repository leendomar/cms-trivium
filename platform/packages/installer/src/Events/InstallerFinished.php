<?php

namespace Trivium\Installer\Events;

use Trivium\Base\Events\Event;

class InstallerFinished extends Event
{
}
