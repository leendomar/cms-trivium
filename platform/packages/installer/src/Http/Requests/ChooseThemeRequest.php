<?php

namespace Trivium\Installer\Http\Requests;

use Trivium\Support\Http\Requests\Request;
use Trivium\Theme\Facades\Manager;
use Illuminate\Validation\Rule;

class ChooseThemeRequest extends Request
{
    public function rules(): array
    {
        return [
            'theme' => ['required', 'string', Rule::in(array_keys(Manager::getThemes()))],
        ];
    }
}
