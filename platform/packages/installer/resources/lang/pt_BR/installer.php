<?php

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Instalação',
    'next' => 'Próximo passo',
    'back' => 'Anterior',
    'finish' => 'Instalar',
    'installation' => 'Instalação',
    'forms' => [
        'errorTitle' => 'Ocorreram os seguintes erros:',
    ],

    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title' => 'Bem-vindo',
        'message' => 'Antes de começar, precisamos de algumas informações sobre o banco de dados. Você precisará saber os seguintes itens antes de prosseguir.',
        'language' => 'Idioma',
        'next' => 'Vamos lá',
    ],

    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Requisitos do servidor',
        'next' => 'Verificar permissões',
    ],

    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'next' => 'Configurar ambiente',
    ],
    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'wizard' => [
            'title' => 'Configurações do ambiente',
            'form' => [
                'name_required' => 'Um nome de ambiente é necessário.',
                'app_name_label' => 'Título do site',
                'app_name_placeholder' => 'Título do site',
                'app_url_label' => 'URL',
                'app_url_placeholder' => 'URL',
                'db_connection_label' => 'Conexão do banco de dados',
                'db_connection_label_mysql' => 'MySQL',
                'db_connection_label_sqlite' => 'SQLite',
                'db_connection_label_pgsql' => 'PostgreSQL',
                'db_host_label' => 'Host do banco de dados',
                'db_host_placeholder' => 'Host do banco de dados',
                'db_port_label' => 'Porta do banco de dados',
                'db_port_placeholder' => 'Porta do banco de dados',
                'db_name_label' => 'Nome do banco de dados',
                'db_name_placeholder' => 'Nome do banco de dados',
                'db_username_label' => 'Nome de usuário do banco de dados',
                'db_username_placeholder' => 'Nome de usuário do banco de dados',
                'db_password_label' => 'Senha do banco de dados',
                'db_password_placeholder' => 'Senha do banco de dados',
                'buttons' => [
                    'install' => 'Instalar',
                ],
                'db_host_helper' => 'Se você usa o Laravel Sail, basta alterar DB_HOST para DB_HOST=mysql. Em algumas hospedagens DB_HOST pode ser localhost em vez de 127.0.0.1',
                'db_connections' => [
                    'mysql' => 'MySQL',
                    'sqlite' => 'SQLite',
                    'pgsql' => 'PostgreSQL',
                ],
            ],
        ],
        'success' => 'As configurações do seu arquivo .env foram salvas.',
        'errors' => 'Não foi possível salvar o arquivo .env. Crie-o manualmente.',
    ],

    'theme' => [
        'title' => 'Selecione o tema',
        'message' => 'Selecione um tema para personalizar a aparência do seu site. Esta seleção também importará dados de amostra adaptados ao tema escolhido.',
    ],

    /**
     * Create account page.
     */
    'createAccount' => [
        'title' => 'Criar conta',
        'form' => [
            'first_name' => 'Primeiro nome',
            'last_name' => 'Último nome',
            'username' => 'Nome de usuário',
            'email' => 'E-mail',
            'password' => 'Senha',
            'password_confirmation' => 'Confirmação de senha',
            'create' => 'Criar',
        ],
    ],

    /**
     * License page.
     */

    'license' => [
        'title' => 'Ativar licença',
        'skip' => 'Pular por enquanto',
    ],

    'install' => 'Install',

    'final' => [
        'pageTitle' => 'Installation Finished',
        'title' => 'Done',
        'message' => 'Application has been successfully installed.',
        'exit' => 'Go to admin dashboard',
    ],
    'install_success' => 'Instalado com sucesso!',

    'install_step_title' => 'Instalação - Etapa :step: :title',
];
