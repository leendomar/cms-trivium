<?php

return [
    'drag_drop_info' => 'Arraste e solte na esquerda para alterar a ordem ou pai das categorias.',
    'expand_all' => 'Expandir tudo',
    'collapse_all' => 'Recolher tudo',
    'empty_text' => 'Nenhuma categoria encontrada.',
    'delete_button' => 'Excluir',
    'delete_modal' => [
        'title' => 'Excluir categoria',
        'message' => 'Tem certeza de que deseja excluir esta categoria?',
    ],
];
