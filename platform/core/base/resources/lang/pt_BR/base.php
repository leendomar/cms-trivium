<?php

return [
    'yes' => 'Sim',
    'no' => 'Não',
    'is_default' => 'É padrão?',
    'proc_close_disabled_error' => 'A função proc_close() está desabilitada. Entre em contato com seu provedor de hospedagem para habilitá-la. Ou você pode adicionar a .env: CAN_EXECUTE_COMMAND=false para desabilitar esse recurso.',
    'email_template' => [
        'header' => 'Cabeçalho do modelo de e-mail',
        'footer' => 'Rodapé do modelo de e-mail',
        'site_title' => 'Título do site',
        'site_url' => 'URL do site',
        'site_logo' => 'Logotipo do site',
        'date_time' => 'Data e hora atuais',
        'date_year' => 'Ano atual',
        'site_admin_email' => 'E-mail do administrador do site',
        'twig' => [
            'tag' => [
                'apply' => 'A tag apply permite que você aplique filtros Twig',
                'for' => 'Fazer um loop em cada item em uma sequência',
                'if' => 'A instrução if no Twig é comparável às instruções if do PHP',
            ],
        ],
    ],
    'change_image' => 'Alterar imagem',
    'delete_image' => 'Excluir imagem',
    'preview_image' => 'Visualizar imagem',
    'image' => 'Imagem',
    'using_button' => 'Usando botão',
    'select_image' => 'Selecionar imagem',
    'click_here' => 'Clique aqui',
    'to_add_more_image' => 'para adicionar mais imagens',
    'add_image' => 'Adicionar imagem',
    'tools' => 'Ferramentas',
    'close' => 'Fechar',
    'panel' => [
        'others' => 'Outros',
        'system' => 'Sistema',
        'manage_description' => 'Gerenciar :name',
    ],
    'global_search' => [
        'title' => 'Pesquisar',
        'search' => 'Pesquisar',
        'clear' => 'Limpar',
        'no_result' => 'Nenhum resultado encontrado',
        'to_select' => 'selecionar',
        'to_navigate' => 'navegar',
        'to_close' => 'fechar',
    ],
    'validation' => [
        'email_in_blacklist' => 'O :attribute está na lista negra. Use outro endereço de e-mail.',
        'domain' => 'O :attribute deve ser um domínio válido.',
    ],
    'showing_records' => 'Mostrando :from para :to de :total registros',
    'requires_License_activation' => 'Requer ativação de licença'
];
