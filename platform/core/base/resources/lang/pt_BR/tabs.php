<?php

return [
    'detail' => 'Detalhe',
    'file' => 'Arquivos',
    'record_note' => 'Nota do Registro',
    'revision' => 'Histórico de Revisão',
];
