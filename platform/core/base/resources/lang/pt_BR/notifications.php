<?php

return [
    'notifications' => 'Notificações',
    'mark_as_read' => 'Marcar tudo como lido',
    'clear' => 'Limpar',
    'no_notification_here' => 'Nenhuma notificação aqui',
    'please_check_again_later' => 'Verifique novamente mais tarde',
    'view_more' => 'Ver mais...',
    'view' => 'Ver',
    'next' => 'Próximo',
    'previous' => 'Anterior',
];
