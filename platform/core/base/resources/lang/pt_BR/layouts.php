<?php

return [
    'platform_admin' => 'Administração da Plataforma',
    'dashboard' => 'Painel',
    'widgets' => 'Widgets',
    'plugins' => 'Plugins',
    'settings' => 'Configurações',
    'setting_general' => 'Geral',
    'setting_email' => 'E-mail',
    'system_information' => 'Informações do sistema',

    'theme' => 'Tema',
    'copyright' => 'Copyright :year © :company. Versão :version',
    'profile' => 'Perfil',
    'logout' => 'Sair',
    'no_search_result' => 'Nenhum resultado encontrado, tente com palavras-chave diferentes.',
    'home' => 'Página inicial',
    'search' => 'Procurar',
    'add_new' => 'Adicionar',
    'n_a' => 'N/A',
    'page_loaded_in_time' => 'Página carregada em :time segundos',
    'view_website' => 'Ver site',
    'tools' => 'Ferramentas',
];
