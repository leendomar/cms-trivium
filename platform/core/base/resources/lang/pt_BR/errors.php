<?php

return [
    '401_title' => 'Permissão negada',
    '404_title' => 'A página não pôde ser encontrada',
    '500_title' => 'A página não pôde ser carregada',
    'reasons' => 'Isso pode ter ocorrido por vários motivos',
    'try_again' => 'Tente novamente em alguns minutos ou, alternativamente, retorne à página inicial <a href=":link">clicando aqui</a>.',
    'not_found' => 'Não encontrado',
    'results_not_found' => 'Resultados não encontrados!',
    'go_to_home' => 'Ir para a página inicial',
    'error_when_sending_email' => 'Não é possível enviar e-mail. Algo errado com suas configurações de e-mail em Admin → Configurações → E-mail, verifique suas configurações novamente.',
    'error_when_sending_email_guest' => 'Não é possível enviar e-mail. Entre em contato com o administrador do site se o problema persistir.',
];
