<div class="debug-badge" role="button" data-bs-toggle="modal" data-bs-target="#debug-mode-modal">Modo de depuração</div>

<x-core::modal.action id="debug-mode-modal" type="info" title="Debug Mode" size="md"
    :submit-button-label="__('Fix it for me')"
    :submit-button-attrs="['data-bs-toggle' => 'modal', 'data-bs-target' => '#debug-mode-turn-off-confirmation-modal']"
    submit-button-color="warning">
    <div class="text-start">
        <p>
            Por padrão, esta opção é definida para respeitar o valor da variável de ambiente <code
                class="text-danger">APP_DEBUG</code>, que é armazenada no seu arquivo <code
                class="text-danger">.env</code>.
        </p>
        <p>Para desenvolvimento local, você deve definir a variável de ambiente <code
                class="text-danger">APP_DEBUG</code> como
            <code class="text-danger">true</code>. No seu ambiente de produção, esse valor deve ser sempre <code class="text-danger">false</code>. 
            Se a variável for definida como <code class="text-danger">true</code> na produção, você corre o risco de expor valores de configuração sensíveis aos usuários finais do seu aplicativo.
        </p>
    </div>
</x-core::modal.action>

<x-core::modal.action id="debug-mode-turn-off-confirmation-modal" type="warning" :title="__('Are you sure?')"
    :description="__('Are you sure you want to turn off the debug mode? This action cannot be undone.')"
    :submit-button-label="__('Yes, turn off')"
    :submit-button-attrs="['id' => 'debug-mode-turn-off-form-submit', 'data-url' => route('system.debug-mode.turn-off')]"
    :cancel-button="true"></x-core::modal.action>