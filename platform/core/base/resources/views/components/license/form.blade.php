<x-core::alert type="warning">
    @if ($manageLicense = auth()->guard()->user()->hasPermission('core.manage.license'))
        <div>Sua licença é inválida. Por favor, ative sua licença!</div>
    @else
        <div>Você não tem permissão para ativar a licença!</div>
    @endif
</x-core::alert>

<x-core::form.text-input label="Seu nome de usuário no Trivium Tech" name="buyer" id="buyer"
    placeholder="Seu nome de usuário Trivium Tech" :disabled="!$manageLicense">
</x-core::form.text-input>

<x-core::form.text-input label="Código de compra" name="purchase_code" id="purchase_code" :disabled="!$manageLicense"
    placeholder="Ex: 00000000-0000-0000-0000-000000000000">
</x-core::form.text-input>

<x-core::form.on-off.checkbox name="license_rules_agreement" id="licenseRulesAgreement" :disabled="!$manageLicense">
    Confirme que, de acordo com os Termos de Licença do Trivium Tech, cada licença dá direito a uma pessoa para um único
    projeto. Criar múltiplas instalações não registradas é uma violação de direitos autorais.
    <a href="https://trivium.dev.br/licenses/standard" target="_blank" rel="nofollow">Mais informações</a>.
</x-core::form.on-off.checkbox>

<x-core-setting::form-group>
    <x-core::button type="submit" color="primary" :disabled="!$manageLicense">
        Activate license
    </x-core::button>

    <div class="form-hint">
        <a href="{{ $licenseURL = Trivium\Base\Supports\Core::make()->getLicenseUrl() }}" target="_blank"
            class="d-inline-block mt-2"> Precisa redefinir sua licença?
        </a> <span class="text-body">Por favor, faça login em nosso <a href="{{ $licenseURL }}" target="_blank">licença
                do cliente site do gerente</a> para redefinir sua licença.</span>
    </div>

</x-core-setting::form-group>

<div>
    <p class="text-danger">Observação: o IP do seu site será adicionado à lista negra após 5 tentativas mal sucedidas.
    </p>

    <p>Um código de compra (licença) é válido apenas para um domínio. Você está usando este tema em um novo domínio?
        Compre um
        <a href="{{ Trivium\Base\Supports\Core::make()->getLicenseUrl('/buy') }}" target="_blank" rel="nofollow">
            nova licença aqui
        </a>para obter um novo código de compra.
    </p>
</div>