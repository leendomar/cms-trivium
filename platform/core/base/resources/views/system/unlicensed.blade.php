<x-core::layouts.base body-class="d-flex flex-column" :body-attributes="['data-bs-theme' => 'dark']">
    <x-slot:title>
        @yield('title')
    </x-slot:title>

    <div class="page page-center">
        <div class="container container-tight py-4">
            <div class="text-center mb-4">
                @include('core/base::partials.logo')
            </div>

            <x-core::card size="md">
                <x-core::card.body>
                    <h2 class="mb-3 text-center">{{__('Requer ativação de licença')}}n</h2>

                    <p class="text-secondary mb-4">
                        Se você quiser continuar usando nossa plataforma, ative a licença primeiro.
                    </p>

                    <ul class="list-unstyled space-y">
                        <li class="row g-2">
                            <span class="col-auto">
                                <x-core::icon name="ti ti-check" class="me-1 text-success" />
                            </span>
                            <span class="col">
                                <strong class="d-block">Receba atualizações, SEMPRE!</strong>
                                <span class="d-block text-secondary">Seu site está sempre atualizado.</span>
                            </span>
                        </li>

                        <li class="row g-2">
                            <span class="col-auto">
                                <x-core::icon name="ti ti-check" class="me-1 text-success" />
                            </span>
                            <span class="col">
                                <strong class="d-block">Obtenha suporte da nossa equipe de desenvolvimento</strong>
                                <span class="d-block text-secondary">Você tem um problema. Não se preocupe. Estamos aqui
                                    para ajudar
                                    você sempre que precisar.</span>
                            </span>
                        </li>
                    </ul>

                    <div class="my-2">
                        <x-core::button color="primary" class="w-100" data-bs-toggle="modal"
                            data-bs-target="#quick-activation-license-modal" aria-label="close">
                            Ativar licença
                        </x-core::button>
                    </div>

                    <div>
                        <form action="{{ route('unlicensed.skip') }}" method="POST">
                            @csrf

                            @if($redirectUrl)
                                <input type="hidden" name="redirect_url" value="{{ $redirectUrl}}" />
                            @endif

                            <x-core::button type="submit" class="w-100" color="link" size="sm">Skip</x-core::button>
                        </form>
                    </div>
                </x-core::card.body>
            </x-core::card>
        </div>
    </div>

    @include('core/base::system.partials.license-activation-modal')
</x-core::layouts.base>