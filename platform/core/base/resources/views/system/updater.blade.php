@php
    use Trivium\Base\Enums\SystemUpdaterStepEnum;

    $changelog = isset($latestUpdate) && $latestUpdate && $latestUpdate->changelog ? trim(str_replace(PHP_EOL . PHP_EOL, PHP_EOL, strip_tags(str_replace(['<li>', '</li>', '<ul>'], ['<li>- ', '</li>' . PHP_EOL, PHP_EOL . '<ul>'], $latestUpdate->changelog)))) : ''
@endphp

@extends(BaseHelper::getAdminMasterLayoutTemplate())

@section('content')
<div style="max-width: 900px;" class="m-auto">
    <x-core::alert type="warning" title="Important notes:" :important="false">
        <ul class="mt-3 mb-0 ps-2">
            <li class="mb-2">Faça backup do seu banco de dados e arquivos de script antes de atualizar.</li>
            <li class="mb-2">Você precisa ativar sua licença antes de fazer a atualização.</li>
            <li class="mb-2">Se você não precisa dessa atualização de 1 clique, você pode desabilitá-la em
                <strong>.env</strong>
                adicionando
                <strong>CMS_ENABLE_SYSTEM_UPDATER=false</strong>
            </li>
            <li>Ele substituirá todos os arquivos em <strong>platform/core</strong>, <strong>platform/packages</strong>,
                todos os
                plugins desenvolvidos por nós em <strong>platform/plugins</strong> e tema desenvolvido por nós em
                <strong>platform/themes</strong>.
            </li>
        </ul>
    </x-core::alert>

    @if ($memoryLimit < $requiredMemoryLimit || $maximumExecutionTime < $requiredMaximumExecutionTime)
        <x-core::alert type="warning" title="Warning" :important="false">
            <div class="mt-3 mb-0">
                Seu servidor não atende aos requisitos mínimos para esta atualização, portanto você pode não conseguir
                atualizar para a versão mais recente. Por favor, melhore o seguinte:
                <ul class="mt-3 mb-0 ps-2">
                    @if ($memoryLimit < $requiredMemoryLimit)
                        <li class="mb-2"><code>memory_limit</code>: <code>{{ $memoryLimit }}M</code> =>
                            <code>{{ $requiredMemoryLimit }}M</code>
                        </li>
                    @endif

                    @if ($maximumExecutionTime < $requiredMaximumExecutionTime)
                        <li class="mb-2"><code>max_execution_time</code>: <code>{{ $maximumExecutionTime }}</code>
                            => <code>{{ $requiredMaximumExecutionTime }}</code></li>
                    @endif
                </ul>
            </div>
        </x-core::alert>
    @endif

    @if (!$activated)
        <x-core::alert type="warning" title="You are not activated your license yet!" :important="true">
            <p class="mt-3 mb-0">
                É necessário ativar sua licença antes de fazer a atualização.
                Acesse a página <a href="{{ route('settings.general') }}" class="fw-bold text-white">configurações</a>
                para ativar sua licença.
            </p>
        </x-core::alert>
    @endif

    @if($isOutdated && $latestUpdate)
        <x-core::card class="mb-3">
            <x-core::card.header>
                <x-core::card.title>
                    Último changelog ({{ BaseHelper::formatDate($latestUpdate->releasedDate) }})
                </x-core::card.title>
            </x-core::card.header>
            <x-core::card.body dir="ltr">
                <h3 class="text-success mb-4">
                    {{ __('A new version (:version / released on :date) is available to update!', ['version' => $latestUpdate->version, 'date' => BaseHelper::formatDate($latestUpdate->releasedDate)]) }}
                </h3>

                <pre>{!! $changelog !!}</pre>
            </x-core::card.body>
        </x-core::card>
    @endif

    @if(request()->query('no-ajax') != 1)
        <x-core::card class="mb-3">
            <x-core::card.header>
                <x-core::card.title>Atualizar Sistema em um click</x-core::card.title>
            </x-core::card.header>

            <x-core::card.body dir="ltr">
                @if (!empty($latestUpdate))
                    <system-update-component update-url="{{ route('system.updater.post') }}"
                        update-id="{{ $latestUpdate->updateId }}" version="{{ $latestUpdate->version }}"
                        first-step="{{ SystemUpdaterStepEnum::firstStep() }}"
                        first-step-message="{{ SystemUpdaterStepEnum::DOWNLOAD()->message() }}"
                        last-step="{{ SystemUpdaterStepEnum::lastStep() }}" :is-outdated="{{ Js::encode($isOutdated) }}"
                        :is-activated="{{ Js::encode($activated) }}" v-slot="{ performUpdate }">
                        @if (!$isOutdated)
                            <h3 class="text-success">{{ __('The system is up-to-date. There are no new versions to update!') }}</h3>
                        @endif

                        @if (!$activated)
                            <x-core::modal.action id="system-updater-confirm-modal" type="warning" title="Are you sure?"
                                description="Your license has not been activated yet! You might not receive the latest updates."
                                submit-button-label="Yes, update it!" :submit-button-attrs="['@click' => 'performUpdate']"
                                :cancel-button="true"></x-core::modal.action>
                        @endif
                    </system-update-component>

                    @if(!$isOutdated)
                        <p class="mt-3">
                            Isso não alterará nem redefinirá seus dados; apenas reinstalará a versão mais recente do sistema.
                        </p>
                    @endif
                @else
                    <p class="mb-0 text-success">{{ __('The system is up-to-date. There are no new versions to update!') }}</p>
                @endif
            </x-core::card.body>
        </x-core::card>
    @endif

    @if($latestUpdate)
        <x-core::card class="mb-3">
            <x-core::card.header>
                <x-core::card.title>Manual System Updater</x-core::card.title>
            </x-core::card.header>
            <x-core::card.body dir="ltr">
                <x-core::alert type="info" class="mb-3">
                    Está com problemas com o One-Click System Updater? Não se preocupe! Confira o atualizador manual abaixo - é fácil, basta seguir os passos!
                </x-core::alert>

                <form
                    action="{{ route('system.updater') }}?no-ajax=1&update_id={{ $latestUpdate->updateId }}&version={{ $latestUpdate->version }}"
                    method="POST">
                    <x-core::step :vertical="true" :counter="true">
                        @foreach (SystemUpdaterStepEnum::labels() as $step => $label)
                            @break($step === SystemUpdaterStepEnum::lastStep())

                            <x-core::step.item>
                                <div class="h4 mb-2">{{ $label }}</div>
                                <x-core::button class="mb-3" type="submit" name="step_name" value="{{ $step }}" color="primary"
                                    data-updating-text="{{ __('Updating...') }}">
                                    {{ __('Run') }}
                                </x-core::button>
                            </x-core::step.item>
                        @endforeach
                    </x-core::step>
                    @csrf
                </form>
            </x-core::card.body>
        </x-core::card>
    @endif

    @if (isset($isOutdated) && isset($latestUpdate) && !$isOutdated && $latestUpdate)
        <x-core::card>
            <x-core::card.header>
                <x-core::card.title>
                    Latest changelog ({{ BaseHelper::formatDate($latestUpdate->releasedDate) }})
                </x-core::card.title>
            </x-core::card.header>
            <x-core::card.body>
                <pre>{!! $changelog !!}</pre>
            </x-core::card.body>
        </x-core::card>
    @endif
</div>
@endsection