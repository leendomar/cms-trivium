class CacheManagement {
    init() {
        $(document).on('click', '.btn-clear-cache', (event) => {
            event.preventDefault()

            let _self = $(event.currentTarget)

            Trivium.showButtonLoading(_self)

            $httpClient
                .make()
                .post(_self.data('url'), { type: _self.data('type') })
                .then(({ data }) => Trivium.showSuccess(data.message))
                .finally(() => Trivium.hideButtonLoading(_self))
        })
    }
}

$(() => {
    new CacheManagement().init()
})
