<?php

namespace Trivium\Base\Events;

use Illuminate\Foundation\Events\Dispatchable;

class SystemUpdateExtractingFiles
{
    use Dispatchable;
}
