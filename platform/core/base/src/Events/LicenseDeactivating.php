<?php

namespace Trivium\Base\Events;

use Illuminate\Foundation\Events\Dispatchable;

class LicenseDeactivating
{
    use Dispatchable;
}
