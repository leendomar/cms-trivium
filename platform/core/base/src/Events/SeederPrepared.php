<?php

namespace Trivium\Base\Events;

use Illuminate\Foundation\Events\Dispatchable;

class SeederPrepared extends Event
{
    use Dispatchable;
}
