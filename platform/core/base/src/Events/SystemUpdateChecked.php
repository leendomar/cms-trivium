<?php

namespace Trivium\Base\Events;

use Illuminate\Foundation\Events\Dispatchable;

class SystemUpdateChecked
{
    use Dispatchable;
}
