<?php

namespace Trivium\Base\Models;

use Trivium\Base\Contracts\BaseModel as BaseModelContract;
use Trivium\Base\Facades\MacroableModels;
use Trivium\Base\Models\Concerns\HasBaseEloquentBuilder;
use Trivium\Base\Models\Concerns\HasMetadata;
use Trivium\Base\Models\Concerns\HasUuidsOrIntegerIds;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * @method static \Trivium\Base\Models\BaseQueryBuilder query()
 */
class BaseModel extends Model implements BaseModelContract
{
    use HasBaseEloquentBuilder;
    use HasMetadata;
    use HasUuidsOrIntegerIds;

    public function __get($key)
    {
        if (MacroableModels::modelHasMacro($this::class, $method = 'get' . Str::studly($key) . 'Attribute')) {
            return call_user_func([$this, $method]);
        }

        return parent::__get($key);
    }
}
