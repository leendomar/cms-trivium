<?php

namespace Trivium\Base\Exceptions;

use RuntimeException;

class LicenseInvalidException extends RuntimeException
{
}
