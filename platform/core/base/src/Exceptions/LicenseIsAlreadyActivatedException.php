<?php

namespace Trivium\Base\Exceptions;

use RuntimeException;

class LicenseIsAlreadyActivatedException extends RuntimeException
{
}
