<?php

namespace Trivium\Base\Exceptions;

use Exception;

class FileNotWritableException extends Exception
{
}
