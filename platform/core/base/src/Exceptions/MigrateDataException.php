<?php

namespace Trivium\Base\Exceptions;

use RuntimeException;

class MigrateDataException extends RuntimeException
{
}
