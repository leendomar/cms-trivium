<?php

namespace Trivium\Base\Repositories\Caches;

use Trivium\Base\Repositories\Eloquent\MetaBoxRepository;

/**
 * @deprecated
 */
class MetaBoxCacheDecorator extends MetaBoxRepository
{
}
