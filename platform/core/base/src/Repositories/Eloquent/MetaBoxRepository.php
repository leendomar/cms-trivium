<?php

namespace Trivium\Base\Repositories\Eloquent;

use Trivium\Base\Models\BaseModel;
use Trivium\Base\Models\BaseQueryBuilder;
use Trivium\Base\Models\MetaBox;
use Trivium\Base\Repositories\Interfaces\MetaBoxInterface;
use Trivium\Support\Repositories\Eloquent\RepositoriesAbstract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class MetaBoxRepository extends RepositoriesAbstract implements MetaBoxInterface
{
    public function __construct(protected BaseModel|BaseQueryBuilder|Builder|Model $model)
    {
        parent::__construct(new MetaBox());
    }
}
