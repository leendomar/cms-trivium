<?php

namespace Trivium\Base\Repositories\Interfaces;

use Trivium\Support\Repositories\Interfaces\RepositoryInterface;

interface MetaBoxInterface extends RepositoryInterface
{
}
