<?php

namespace Trivium\Base\Providers;

use Trivium\Base\Commands\ActivateLicenseCommand;
use Trivium\Base\Commands\CleanupSystemCommand;
use Trivium\Base\Commands\ClearExpiredCacheCommand;
use Trivium\Base\Commands\ClearLogCommand;
use Trivium\Base\Commands\ExportDatabaseCommand;
use Trivium\Base\Commands\FetchGoogleFontsCommand;
use Trivium\Base\Commands\ImportDatabaseCommand;
use Trivium\Base\Commands\InstallCommand;
use Trivium\Base\Commands\PublishAssetsCommand;
use Trivium\Base\Commands\UpdateCommand;
use Trivium\Base\Supports\ServiceProvider;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\AboutCommand;

class CommandServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        if (! $this->app->runningInConsole()) {
            return;
        }

        $this->commands([
            ActivateLicenseCommand::class,
            CleanupSystemCommand::class,
            ClearExpiredCacheCommand::class,
            ClearLogCommand::class,
            ExportDatabaseCommand::class,
            FetchGoogleFontsCommand::class,
            ImportDatabaseCommand::class,
            InstallCommand::class,
            PublishAssetsCommand::class,
            UpdateCommand::class,
        ]);

        AboutCommand::add('Core Information', fn () => [
            'CMS Version' => get_cms_version(),
            'Core Version' => get_core_version(),
        ]);

        $this->app->afterResolving(Schedule::class, function (Schedule $schedule) {
            $schedule->command(ClearExpiredCacheCommand::class)->everyFiveMinutes();
        });
    }
}
