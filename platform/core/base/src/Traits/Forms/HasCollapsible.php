<?php

namespace Trivium\Base\Traits\Forms;

use Trivium\Base\Forms\FormCollapse;

trait HasCollapsible
{
    public function addCollapsible(FormCollapse $form): static
    {
        $form->build($this);

        return $this;
    }
}
