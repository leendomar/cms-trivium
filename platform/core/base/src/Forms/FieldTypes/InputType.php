<?php

namespace Trivium\Base\Forms\FieldTypes;

use Trivium\Base\Traits\Forms\CanSpanColumns;

class InputType extends \Kris\LaravelFormBuilder\Fields\InputType
{
    use CanSpanColumns;
}
