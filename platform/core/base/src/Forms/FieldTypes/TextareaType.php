<?php

namespace Trivium\Base\Forms\FieldTypes;

use Trivium\Base\Traits\Forms\CanSpanColumns;

class TextareaType extends \Kris\LaravelFormBuilder\Fields\TextareaType
{
    use CanSpanColumns;
}
