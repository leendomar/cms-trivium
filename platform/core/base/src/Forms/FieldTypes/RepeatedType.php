<?php

namespace Trivium\Base\Forms\FieldTypes;

use Trivium\Base\Traits\Forms\CanSpanColumns;

class RepeatedType extends \Kris\LaravelFormBuilder\Fields\RepeatedType
{
    use CanSpanColumns;
}
