<?php

namespace Trivium\Base\Forms\FieldTypes;

use Trivium\Base\Traits\Forms\CanSpanColumns;

class ButtonType extends \Kris\LaravelFormBuilder\Fields\ButtonType
{
    use CanSpanColumns;
}
