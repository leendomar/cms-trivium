<?php

namespace Trivium\Base\Forms\FieldTypes;

use Trivium\Base\Traits\Forms\CanSpanColumns;

class EntityType extends ChoiceType
{
    use CanSpanColumns;
}
