<?php

namespace Trivium\Base\Forms\FieldTypes;

use Trivium\Base\Traits\Forms\CanSpanColumns;

class ButtonGroupType extends \Kris\LaravelFormBuilder\Fields\ButtonGroupType
{
    use CanSpanColumns;
}
