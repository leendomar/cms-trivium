<?php

namespace Trivium\Base\Forms\FieldTypes;

use Trivium\Base\Traits\Forms\CanSpanColumns;

abstract class FormField extends \Kris\LaravelFormBuilder\Fields\FormField
{
    use CanSpanColumns;
}
