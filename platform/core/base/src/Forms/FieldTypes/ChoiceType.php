<?php

namespace Trivium\Base\Forms\FieldTypes;

use Trivium\Base\Traits\Forms\CanSpanColumns;

class ChoiceType extends \Kris\LaravelFormBuilder\Fields\ChoiceType
{
    use CanSpanColumns;
}
