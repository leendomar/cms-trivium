<?php

namespace Trivium\Base\Forms\FieldTypes;

use Trivium\Base\Traits\Forms\CanSpanColumns;

class ChildFormType extends \Kris\LaravelFormBuilder\Fields\ChildFormType
{
    use CanSpanColumns;
}
