<?php

namespace Trivium\Base\Forms\Fields;

use Trivium\Base\Facades\Assets;
use Trivium\Base\Forms\FormField;

class MediaImagesField extends FormField
{
    protected function getTemplate(): string
    {
        Assets::addScripts(['jquery-ui']);

        return 'core/base::forms.fields.media-images';
    }
}
