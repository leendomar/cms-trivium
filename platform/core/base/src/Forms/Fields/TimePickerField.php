<?php

namespace Trivium\Base\Forms\Fields;

use Trivium\Base\Facades\Assets;
use Trivium\Base\Forms\FormField;

class TimePickerField extends FormField
{
    protected function getTemplate(): string
    {
        Assets::addScripts(['timepicker'])
            ->addStyles(['timepicker']);

        return 'core/base::forms.fields.time-picker';
    }
}
