<?php

namespace Trivium\Base\Forms\Fields;

use Trivium\Base\Forms\FormField;

class PasswordField extends FormField
{
    protected function getTemplate(): string
    {
        return 'core/base::forms.fields.password';
    }
}
