<?php

namespace Trivium\Base\Forms\Fields;

use Trivium\Base\Forms\FormField;

class CodeEditorField extends FormField
{
    protected function getTemplate(): string
    {
        return 'core/base::forms.fields.code-editor';
    }
}
