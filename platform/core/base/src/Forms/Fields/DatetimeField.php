<?php

namespace Trivium\Base\Forms\Fields;

use Trivium\Base\Forms\FormField;

class DatetimeField extends FormField
{
    protected function getTemplate(): string
    {
        return 'core/base::forms.fields.datetime';
    }
}
