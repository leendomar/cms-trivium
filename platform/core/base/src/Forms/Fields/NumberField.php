<?php

namespace Trivium\Base\Forms\Fields;

use Trivium\Base\Forms\FormField;

class NumberField extends FormField
{
    protected function getTemplate(): string
    {
        return 'core/base::forms.fields.number';
    }
}
