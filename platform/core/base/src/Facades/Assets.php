<?php

namespace Trivium\Base\Facades;

use Trivium\Base\Supports\Assets as BaseAssets;
use Illuminate\Support\Facades\Facade;

/**
 * @method static void setConfig(array $config)
 * @method static string renderHeader($lastStyles = [])
 * @method static string renderFooter()
 * @method static \Trivium\Base\Supports\Assets usingVueJS()
 * @method static \Trivium\Base\Supports\Assets disableVueJS()
 * @method static bool hasVueJs()
 * @method static \Trivium\Assets\Assets addScripts(string|array $assets)
 * @method static \Trivium\Assets\Assets addStyles(string|array $assets)
 * @method static \Trivium\Assets\Assets addStylesDirectly(array|string $assets)
 * @method static \Trivium\Assets\Assets addScriptsDirectly(string|array $assets, string $location = 'footer')
 * @method static \Trivium\Assets\Assets removeStyles(string|array $assets)
 * @method static \Trivium\Assets\Assets removeScripts(string|array $assets)
 * @method static \Trivium\Assets\Assets removeItemDirectly(string|array $assets, string|null $location = null)
 * @method static array getScripts(string|null $location = null)
 * @method static array getStyles(array $lastStyles = [])
 * @method static string|null scriptToHtml(string $name)
 * @method static string|null styleToHtml(string $name)
 * @method static string getBuildVersion()
 * @method static \Trivium\Assets\HtmlBuilder getHtmlBuilder()
 *
 * @see \Trivium\Base\Supports\Assets
 */
class Assets extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return BaseAssets::class;
    }
}
