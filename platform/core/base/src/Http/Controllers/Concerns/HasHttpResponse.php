<?php

namespace Trivium\Base\Http\Controllers\Concerns;

use Trivium\Base\Http\Responses\BaseHttpResponse;

trait HasHttpResponse
{
    public function httpResponse(): BaseHttpResponse
    {
        return BaseHttpResponse::make();
    }
}
