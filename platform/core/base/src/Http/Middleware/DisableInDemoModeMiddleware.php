<?php

namespace Trivium\Base\Http\Middleware;

use Trivium\Base\Exceptions\DisabledInDemoModeException;
use Trivium\Base\Facades\BaseHelper;
use Closure;
use Illuminate\Http\Request;

class DisableInDemoModeMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if (BaseHelper::hasDemoModeEnabled()) {
            throw new DisabledInDemoModeException();
        }

        return $next($request);
    }
}
