<?php

namespace Trivium\Icon\Facades;

use Trivium\Icon\IconManager;
use Illuminate\Support\Facades\Facade;

/**
 * @method static string getDefaultDriver()
 * @method static \Trivium\Icon\IconDriver createSvgDriver()
 * @method static mixed driver(string|null $driver = null)
 * @method static \Trivium\Icon\IconManager extend(string $driver, \Closure $callback)
 * @method static array getDrivers()
 * @method static \Illuminate\Contracts\Container\Container getContainer()
 * @method static \Trivium\Icon\IconManager setContainer(\Illuminate\Contracts\Container\Container $container)
 * @method static \Trivium\Icon\IconManager forgetDrivers()
 *
 * @see \Trivium\Icon\IconManager
 */
class Icon extends Facade
{
    /**
     * Get the registered name of the component.
     */
    protected static function getFacadeAccessor(): string
    {
        return IconManager::class;
    }
}
