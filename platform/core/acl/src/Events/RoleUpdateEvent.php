<?php

namespace Trivium\ACL\Events;

use Trivium\ACL\Models\Role;
use Trivium\Base\Events\Event;
use Illuminate\Queue\SerializesModels;

class RoleUpdateEvent extends Event
{
    use SerializesModels;

    public function __construct(public Role $role)
    {
    }
}
