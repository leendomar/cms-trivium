<?php

namespace Trivium\ACL\Events;

use Trivium\ACL\Models\Role;
use Trivium\ACL\Models\User;
use Trivium\Base\Events\Event;
use Illuminate\Queue\SerializesModels;

class RoleAssignmentEvent extends Event
{
    use SerializesModels;

    public function __construct(public Role $role, public User $user)
    {
    }
}
