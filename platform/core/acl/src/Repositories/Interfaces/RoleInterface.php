<?php

namespace Trivium\ACL\Repositories\Interfaces;

use Trivium\Support\Repositories\Interfaces\RepositoryInterface;

interface RoleInterface extends RepositoryInterface
{
    public function createSlug(string $name, int|string $id): string;
}
