<?php

namespace Trivium\ACL\Repositories\Interfaces;

use Trivium\Support\Repositories\Interfaces\RepositoryInterface;

interface UserInterface extends RepositoryInterface
{
    public function getUniqueUsernameFromEmail(string $email): string;
}
