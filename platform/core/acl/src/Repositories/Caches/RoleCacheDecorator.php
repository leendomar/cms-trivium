<?php

namespace Trivium\ACL\Repositories\Caches;

use Trivium\ACL\Repositories\Eloquent\RoleRepository;

/**
 * @deprecated
 */
class RoleCacheDecorator extends RoleRepository
{
}
