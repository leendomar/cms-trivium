<?php

namespace Trivium\ACL\Tables;

use Trivium\ACL\Models\Role;
use Trivium\Base\Facades\BaseHelper;
use Trivium\Table\Abstracts\TableAbstract;
use Trivium\Table\Actions\DeleteAction;
use Trivium\Table\Actions\EditAction;
use Trivium\Table\BulkActions\DeleteBulkAction;
use Trivium\Table\BulkChanges\NameBulkChange;
use Trivium\Table\Columns\CreatedAtColumn;
use Trivium\Table\Columns\FormattedColumn;
use Trivium\Table\Columns\IdColumn;
use Trivium\Table\Columns\LinkableColumn;
use Trivium\Table\Columns\NameColumn;
use Trivium\Table\HeaderActions\CreateHeaderAction;
use Illuminate\Database\Eloquent\Builder;

class RoleTable extends TableAbstract
{
    public function setup(): void
    {
        $this
            ->model(Role::class)
            ->addColumns([
                IdColumn::make(),
                NameColumn::make()->route('roles.edit'),
                FormattedColumn::make('description')
                    ->title(trans('core/base::tables.description'))
                    ->alignStart()
                    ->withEmptyState(),
                CreatedAtColumn::make(),
                LinkableColumn::make('created_by')
                    ->urlUsing(fn (LinkableColumn $column) => $column->getItem()->author->url)
                    ->title(trans('core/acl::permissions.created_by'))
                    ->width(100)
                    ->getValueUsing(function (LinkableColumn $column) {
                        return BaseHelper::clean($column->getItem()->author->name);
                    })
                    ->externalLink()
                    ->withEmptyState(),
            ])
            ->addHeaderAction(CreateHeaderAction::make()->route('roles.create'))
            ->addActions([
                EditAction::make()->route('roles.edit'),
                DeleteAction::make()->route('roles.destroy'),
            ])
            ->addBulkAction(DeleteBulkAction::make()->permission('roles.destroy'))
            ->addBulkChange(NameBulkChange::make())
            ->queryUsing(function (Builder $query) {
                $query
                    ->with('author')
                    ->select([
                        'id',
                        'name',
                        'description',
                        'created_at',
                        'created_by',
                    ]);
            });
    }
}
