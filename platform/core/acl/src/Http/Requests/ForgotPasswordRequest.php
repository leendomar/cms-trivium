<?php

namespace Trivium\ACL\Http\Requests;

use Trivium\Base\Rules\EmailRule;
use Trivium\Support\Http\Requests\Request;

class ForgotPasswordRequest extends Request
{
    public function rules(): array
    {
        return [
            'email' => ['required', new EmailRule()],
        ];
    }
}
