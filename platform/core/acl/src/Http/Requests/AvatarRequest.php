<?php

namespace Trivium\ACL\Http\Requests;

use Trivium\Media\Facades\RvMedia;
use Trivium\Support\Http\Requests\Request;

class AvatarRequest extends Request
{
    public function rules(): array
    {
        return [
            'avatar_file' => RvMedia::imageValidationRule(),
        ];
    }
}
