<?php

namespace Trivium\ACL\Http\Requests;

use Trivium\Base\Rules\EmailRule;
use Trivium\Support\Http\Requests\Request;

class ResetPasswordRequest extends Request
{
    public function rules(): array
    {
        return [
            'token' => ['required', 'string'],
            'email' => ['required', new EmailRule()],
            'password' => ['required', 'confirmed', 'min:6'],
        ];
    }
}
