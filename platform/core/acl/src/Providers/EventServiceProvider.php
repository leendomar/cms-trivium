<?php

namespace Trivium\ACL\Providers;

use Trivium\ACL\Events\RoleAssignmentEvent;
use Trivium\ACL\Events\RoleUpdateEvent;
use Trivium\ACL\Listeners\LoginListener;
use Trivium\ACL\Listeners\RoleAssignmentListener;
use Trivium\ACL\Listeners\RoleUpdateListener;
use Illuminate\Auth\Events\Login;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        RoleUpdateEvent::class => [
            RoleUpdateListener::class,
        ],
        RoleAssignmentEvent::class => [
            RoleAssignmentListener::class,
        ],
        Login::class => [
            LoginListener::class,
        ],
    ];
}
