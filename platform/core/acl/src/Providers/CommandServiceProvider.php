<?php

namespace Trivium\ACL\Providers;

use Trivium\ACL\Commands\UserCreateCommand;
use Trivium\Base\Supports\ServiceProvider;

class CommandServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        if (! $this->app->runningInConsole()) {
            return;
        }

        $this->commands([
            UserCreateCommand::class,
        ]);
    }
}
