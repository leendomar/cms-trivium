<?php

return [
    'login' => [
        'username' => 'E-mail/Nome de usuário',
        'email' => 'E-mail',
        'password' => 'Senha',
        'title' => 'Login do usuário',
        'remember' => 'Lembrar de mim?',
        'login' => 'Entrar',
        'placeholder' => [
            'username' => 'Digite seu nome de usuário ou endereço de e-mail',
            'email' => 'Digite seu endereço de e-mail',
            'password' => 'Digite sua senha',
        ],
        'success' => 'Login com sucesso!',
        'fail' => 'Nome de usuário ou senha incorretos.',
        'not_active' => 'Sua conta ainda não foi ativada!',
        'banned' => 'Esta conta foi banida.',
        'logout_success' => 'Logout com sucesso!',
        'dont_have_account' => 'Você não tem conta neste sistema, entre em contato com o administrador para mais informações!',
    ],
    'forgot_password' => [
        'title' => 'Esqueceu a senha',
        'message' => '<p>Você esqueceu sua senha?</p><p>Por favor, informe sua conta de e-mail. O sistema enviará um e-mail com um link ativo para redefinir sua senha.</p>',
        'submit' => 'Enviar',
    ],
    'reset' => [
        'new_password' => 'Nova senha',
        'password_confirmation' => 'Confirmar nova senha',
        'email' => 'E-mail',
        'title' => 'Redefinir sua senha',
        'update' => 'Atualizar',
        'wrong_token' => 'Este link é inválido ou expirou. Tente usar o formulário de redefinição novamente.',
        'user_not_found' => 'Este nome de usuário não existe.',
        'success' => 'Redefinição de senha com sucesso!',
        'fail' => 'Token inválido, o link de redefinição de senha expirou!',
        'reset' => [
            'title' => 'E-mail para redefinição de senha',
        ],
        'send' => [
            'success' => 'Um e-mail foi enviado para sua conta de e-mail. Verifique e conclua esta ação.',
            'fail' => 'Não é possível enviar e-mail neste momento. Tente novamente mais tarde.',
        ],
        'new-password' => 'Nova senha',
        'placeholder' => [
            'new_password' => 'Digite sua nova senha',
            'new_password_confirmation' => 'Confirme sua nova senha',
        ],
    ],
    'email' => [
        'reminder' => [
            'title' => 'E-mail para redefinir senha',
        ],
    ],
    'password_confirmation' => 'Confirmação de senha',
    'failed' => 'Falha',
    'throttle' => 'Acelerador',
    'not_member' => 'Ainda não é membro?',
    'register_now' => 'Registre-se agora',
    'lost_your_password' => 'Perdeu sua senha?',
    'login_title' => 'Admin',
    'login_via_social' => 'Entrar com redes sociais',
    'back_to_login' => 'Voltar para a página de login',
    'sign_in_below' => 'Entrar abaixo',
    'languages' => 'Idiomas',
    'reset_password' => 'Redefinir senha',
    'settings' => [
        'email' => [
            'title' => 'ACL',
            'description' => 'Configuração de e-mail ACL',
        ],
    ],
];
