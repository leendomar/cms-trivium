{{ header }}

<strong>Olá!</strong> <br/><br/>
Você está recebendo este e-mail porque recebemos uma solicitação de redefinição de senha para sua conta.<br/><br/>
<a href="{{ reset_link }}">Redefinir senha</a> <br/><br/>
Se você não solicitou uma redefinição de senha, nenhuma outra ação é necessária.<br/><br/>

Atenciosamente <br/>

<strong>{{ site_title }}</strong>

<hr />

Se você estiver com problemas para clicar no botão "Redefinir senha", copie e cole o URL abaixo no seu navegador: {{ reset_link }}

{{ footer }}
