<?php

namespace Trivium\Setting\Repositories\Caches;

use Trivium\Setting\Repositories\Eloquent\SettingRepository;

/**
 * @deprecated
 */
class SettingCacheDecorator extends SettingRepository
{
}
