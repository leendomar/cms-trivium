<?php

namespace Trivium\Setting\Repositories\Eloquent;

use Trivium\Setting\Repositories\Interfaces\SettingInterface;
use Trivium\Support\Repositories\Eloquent\RepositoriesAbstract;

class SettingRepository extends RepositoriesAbstract implements SettingInterface
{
}
