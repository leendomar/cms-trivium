<?php

namespace Trivium\Setting\Repositories\Interfaces;

use Trivium\Support\Repositories\Interfaces\RepositoryInterface;

interface SettingInterface extends RepositoryInterface
{
}
