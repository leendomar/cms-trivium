<?php

namespace Trivium\Setting\Http\Requests;

use Trivium\Base\Rules\OnOffRule;
use Trivium\Support\Http\Requests\Request;

class EmailTemplateChangeStatusRequest extends Request
{
    public function rules(): array
    {
        return [
            'key' => ['required', 'string'],
            'value' => [new OnOffRule()],
        ];
    }
}
