<?php

namespace Trivium\Setting\Http\Controllers;

use Trivium\Base\Http\Responses\BaseHttpResponse;
use Trivium\Setting\Forms\DataTableSettingForm;
use Trivium\Setting\Http\Requests\DataTableSettingRequest;

class DataTableSettingController extends SettingController
{
    public function edit()
    {
        $this->pageTitle(trans('core/setting::setting.datatable.title'));

        return DataTableSettingForm::create()->renderForm();
    }

    public function update(DataTableSettingRequest $request): BaseHttpResponse
    {
        return $this->performUpdate($request->validated());
    }
}
