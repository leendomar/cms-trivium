<?php

namespace Trivium\Setting\Http\Controllers;

use Trivium\Base\Facades\Assets;
use Trivium\Base\Http\Responses\BaseHttpResponse;
use Trivium\Setting\Forms\EmailSettingForm;
use Trivium\Setting\Http\Requests\EmailSettingRequest;

class EmailSettingController extends SettingController
{
    public function edit()
    {
        $this->pageTitle(trans('core/setting::setting.panel.email'));

        Assets::addScriptsDirectly('vendor/core/core/setting/js/email-template.js');

        $form = EmailSettingForm::create();

        return view('core/setting::email', compact('form'));
    }

    public function update(EmailSettingRequest $request): BaseHttpResponse
    {
        return $this->performUpdate($request->validated());
    }
}
