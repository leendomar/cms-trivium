<?php

namespace Trivium\Setting\Http\Controllers;

use Trivium\Base\Http\Responses\BaseHttpResponse;
use Trivium\Setting\Forms\CacheSettingForm;
use Trivium\Setting\Http\Requests\CacheSettingRequest;

class CacheSettingController extends SettingController
{
    public function edit()
    {
        $this->pageTitle(trans('core/setting::setting.cache.title'));

        return CacheSettingForm::create()->renderForm();
    }

    public function update(CacheSettingRequest $request): BaseHttpResponse
    {
        return $this->performUpdate($request->validated());
    }
}
