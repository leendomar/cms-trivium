<?php

namespace Trivium\Setting\Http\Controllers;

use Trivium\Base\Http\Controllers\BaseController;
use Trivium\Base\Supports\Breadcrumb;
use Trivium\Setting\Http\Controllers\Concerns\InteractsWithSettings;

abstract class SettingController extends BaseController
{
    use InteractsWithSettings;

    protected function breadcrumb(): Breadcrumb
    {
        return parent::breadcrumb()
            ->add(trans('core/setting::setting.title'), route('settings.index'));
    }
}
