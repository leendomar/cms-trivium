<?php

namespace Trivium\Dashboard\Repositories\Interfaces;

use Trivium\Support\Repositories\Interfaces\RepositoryInterface;

interface DashboardWidgetInterface extends RepositoryInterface
{
}
