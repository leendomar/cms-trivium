<?php

namespace Trivium\Dashboard\Repositories\Eloquent;

use Trivium\Dashboard\Repositories\Interfaces\DashboardWidgetInterface;
use Trivium\Support\Repositories\Eloquent\RepositoriesAbstract;

class DashboardWidgetRepository extends RepositoriesAbstract implements DashboardWidgetInterface
{
}
