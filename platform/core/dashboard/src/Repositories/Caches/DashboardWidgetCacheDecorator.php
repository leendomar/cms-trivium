<?php

namespace Trivium\Dashboard\Repositories\Caches;

use Trivium\Dashboard\Repositories\Eloquent\DashboardWidgetRepository;

/**
 * @deprecated
 */
class DashboardWidgetCacheDecorator extends DashboardWidgetRepository
{
}
