<?php

namespace Trivium\Dashboard\Repositories\Caches;

use Trivium\Dashboard\Repositories\Eloquent\DashboardWidgetSettingRepository;

/**
 * @deprecated
 */
class DashboardWidgetSettingCacheDecorator extends DashboardWidgetSettingRepository
{
}
