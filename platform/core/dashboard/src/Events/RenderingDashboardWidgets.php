<?php

namespace Trivium\Dashboard\Events;

use Trivium\Base\Events\Event;
use Illuminate\Foundation\Events\Dispatchable;

class RenderingDashboardWidgets extends Event
{
    use Dispatchable;
}
