<?php

return [
    'update_position_success' => 'A posição do widget foi atualizada com sucesso!',
    'hide_success' => 'Atualizado widget com sucesso!',
    'confirm_hide' => 'Tem certeza?',
    'hide_message' => 'Você realmente quer esconder este widget? Ele desaparecerá do Dashboard!',
    'confirm_hide_btn' => 'Sim, ocultar este widget',
    'cancel_hide_btn' => 'Cancelar',
    'collapse_expand' => 'Recolher/Expandir',
    'hide' => 'Ocultar',
    'reload' => 'Reload',
    'save_setting_success' => 'As configurações do widget foram salvas com sucesso!',
    'widget_not_exists' => 'O widget não existe!',
    'manage_widgets' => 'Gerenciar Widgets',
    'fullscreen' => 'Tela cheia',
    'title' => 'Painel',
    'predefined_ranges' => [
        'today' => 'Hoje',
        'yesterday' => 'Ontem',
        'this_week' => 'Essa semana',
        'last_7_days' => 'Últimos 7 dias',
        'this_month' => 'Este mês',
        'last_30_days' => 'Últimos 30 dias',
        'this_year' => 'Este ano',
    ],
];
