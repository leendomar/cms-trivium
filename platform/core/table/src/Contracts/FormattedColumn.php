<?php

namespace Trivium\Table\Contracts;

use Trivium\Base\Contracts\BaseModel;
use Trivium\Table\Abstracts\TableAbstract;

interface FormattedColumn
{
    public function formattedValue($value): string|null;

    public function renderCell(BaseModel|array $item, TableAbstract $table): string;
}
