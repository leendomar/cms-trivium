<?php

namespace Trivium\Table\Providers;

use Trivium\Base\Supports\ServiceProvider;
use Trivium\Base\Traits\LoadAndPublishDataTrait;
use Trivium\Table\ApiResourceDataTable;
use Trivium\Table\CollectionDataTable;
use Trivium\Table\EloquentDataTable;
use Trivium\Table\QueryDataTable;

class TableServiceProvider extends ServiceProvider
{
    use LoadAndPublishDataTrait;

    public function boot(): void
    {
        $this
            ->setNamespace('core/table')
            ->loadHelpers()
            ->loadAndPublishViews()
            ->loadAndPublishTranslations()
            ->loadRoutes()
            ->publishAssets();

        $this->app['config']->set([
            'datatables.engines' => [
                'eloquent' => EloquentDataTable::class,
                'query' => QueryDataTable::class,
                'collection' => CollectionDataTable::class,
                'resource' => ApiResourceDataTable::class,
            ],
        ]);
    }
}
