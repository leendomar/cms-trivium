<?php

namespace Trivium\Table\Actions;

use Trivium\Base\Supports\Builders\HasAttributes;
use Trivium\Base\Supports\Builders\HasColor;
use Trivium\Base\Supports\Builders\HasIcon;
use Trivium\Base\Supports\Builders\HasUrl;
use Trivium\Table\Abstracts\TableActionAbstract;
use Trivium\Table\Actions\Concerns\HasAction;

class Action extends TableActionAbstract
{
    use HasAction;
    use HasAttributes;
    use HasColor;
    use HasIcon;
    use HasUrl;
}
