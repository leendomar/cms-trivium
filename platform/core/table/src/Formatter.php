<?php

namespace Trivium\Table;

use Yajra\DataTables\Contracts\Formatter as BaseFormatter;

interface Formatter extends BaseFormatter
{
}
