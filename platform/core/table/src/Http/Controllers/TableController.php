<?php

namespace Trivium\Table\Http\Controllers;

use Trivium\Base\Http\Controllers\BaseController;
use Trivium\Table\TableBuilder;

class TableController extends BaseController
{
    public function __construct(protected TableBuilder $tableBuilder)
    {
    }
}
