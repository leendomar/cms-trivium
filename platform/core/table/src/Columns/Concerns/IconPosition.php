<?php

namespace Trivium\Table\Columns\Concerns;

enum IconPosition: string
{
    case Start = 'start';

    case End = 'end';
}
