<?php

return [
    'filter' => 'Filtrar',
    'everything' => 'Tudo',
    'image' => 'Imagem',
    'video' => 'Vídeo',
    'document' => 'Documento',
    'view_in' => 'Visualizar em',
    'all_media' => 'Todas as mídias',
    'trash' => 'Lixeira',
    'recent' => 'Recentes',
    'favorites' => 'Favoritos',
    'upload' => 'Carregar',
    'create_folder' => 'Criar pasta',
    'refresh' => 'Atualizar',
    'empty_trash' => 'Esvaziar lixeira',
    'search_file_and_folder' => 'Pesquisar na pasta atual',
    'sort' => 'Classificar',
    'file_name_asc' => 'Nome do arquivo - ASC',
    'file_name_desc' => 'Nome do arquivo - DESC',
    'uploaded_date_asc' => 'Data do upload - ASC',
    'uploaded_date_desc' => 'Data do upload - DESC',
    'size_asc' => 'Tamanho - ASC',
    'size_desc' => 'Tamanho - DESC',
    'actions' => 'Ações',
    'nothing_is_selected' => 'Nada está selecionado',
    'insert' => 'Inserir',
    'folder_name' => 'Nome da pasta',
    'create' => 'Criar',
    'rename' => 'Renomear',
    'crop' => 'Cortar',
    'close' => 'Fechar',
    'save_changes' => 'Salvar alterações',
    'move_to_trash' => 'Mover itens para a lixeira',
    'confirm_trash' => 'Você está tem certeza de que deseja mover esses itens para a lixeira?',
    'confirm' => 'Confirmar',
    'confirm_delete' => 'Excluir item(ns)',
    'confirm_delete_description' => 'Sua solicitação não pode ser revertida. Tem certeza de que deseja excluir esses itens?',
    'empty_trash_title' => 'Esvaziar lixeira',
    'empty_trash_description' => 'Sua solicitação não pode ser revertida. Tem certeza de que deseja remover todos os itens da lixeira?',
    'up_level' => 'Subir um nível',
    'upload_progress' => 'Progresso do upload',
    'alt_text' => 'Texto alternativo',

    'folder_created' => 'Pasta criada com sucesso!',
    'gallery' => 'Galeria de mídia',

    'trash_error' => 'Erro ao excluir item(ns) selecionado(s)',
    'trash_success' => 'Itens selecionados movidos para a lixeira com sucesso!',
    'restore_error' => 'Erro ao restaurar item(ns) selecionado(s)',
    'restore_success' => 'Itens selecionados restaurados com sucesso!',
    'copy_success' => 'Itens selecionados copiados com sucesso!',
    'delete_success' => 'Itens selecionados excluídos com sucesso!',
    'favorite_success' => 'Item(ns) selecionado(s) favorito(s) com sucesso!',
    'remove_favorite_success' => 'Itens selecionados removidos dos favoritos com sucesso!',
    'rename_error' => 'Erro ao renomear item(ns)',
    'rename_success' => 'Renomear item(ns) selecionado(s) com sucesso!',
    'crop_success' => 'Imagem recortada com sucesso!',
    'empty_trash_success' => 'Esvaziar lixeira com sucesso!',
    'invalid_action' => 'Ação inválida!',
    'file_not_exists' => 'O arquivo não existe!',
    'download_file_error' => 'Erro ao baixar arquivos!',
    'missing_zip_archive_extension' => 'Por favor, habilite a extensão ZipArchive para baixar o arquivo!',
    'can_not_download_file' => 'Não é possível baixar este arquivo!',
    'invalid_request' => 'Solicitação inválida!',
    'add_success' => 'Adicionou item com sucesso!',
    'file_too_big' => 'Arquivo muito grande. O upload máximo do arquivo é :size bytes',
    'file_too_big_readable_size' => 'Arquivo muito grande. O upload máximo do arquivo é :size.',
    'can_not_detect_file_type' => 'O tipo de arquivo não é permitido ou não é possível detectar o tipo de arquivo!',
    'upload_failed' => 'O arquivo NÃO foi carregado completamente. O servidor permite que o tamanho máximo do arquivo para upload seja :size . Verifique o tamanho do arquivo OU tente fazer upload novamente em caso de erros de rede',
    'failed_to_crop_image' => 'O corte do arquivo deve ser do tipo imagem',
    'menu_name' => 'Mídia',
    'description' => 'Gerenciar e visualizar arquivos de mídia',
    'panel' => [
        'title' => 'Gerenciar arquivos de mídia',
        'description' => 'Gerenciar e visualizar arquivos de mídia',
    ],
    'add' => 'Adicionar mídia',

    'javascript' => [
        'name' => 'Nome',
        'url' => 'URL',
        'full_url' => 'URL completa',
        'alt' => 'Texto alternativo',
        'size' => 'Tamanho',
        'mime_type' => 'Tipo',
        'created_at' => 'Carregado em',
        'updated_at' => 'Modificado at',
        'nothing_selected' => 'Nada está selecionado',
        'visit_link' => 'Abrir link',
        'width' => 'Largura',
        'height' => 'Altura',

        'no_item' => [
            'all_media' => [
                'title' => 'Solte arquivos e pastas aqui',
                'message' => 'Ou use o botão de upload acima',
            ],
            'trash' => [
                'title' => 'Não há nada na sua lixeira no momento',
                'message' => 'Exclua arquivos para movê-los para a lixeira automaticamente. Exclua arquivos da lixeira para removê-los permanentemente',
            ],
            'favorites' => [
                'title' => 'Você ainda não adicionou nada aos seus favoritos',
                'message' => 'Adicione arquivos aos favoritos para encontrá-los facilmente mais tarde',
            ],
            'recent' => [
                'title' => 'Você ainda não abriu nada',
                'message' => 'Todos os arquivos recentes que você abriu aparecerão aqui',
            ],
            'default' => [
                'title' => 'Nenhum item',
                'message' => 'Este diretório não tem nenhum item',
            ],
        ],

        'clipboard' => [
            'success' => 'Esses links de arquivo foram copiados para a área de transferência',
        ],

        'message' => [
            'error_header' => 'Erro',
            'success_header' => 'Sucesso',
        ],

        'download' => [
            'error' => 'Nenhum arquivo selecionado ou não é possível baixar esses arquivos',
        ],

        'actions_list' => [
            'basic' => [
                'preview' => 'Visualizar',
                'crop' => 'Cortar',
            ],
            'file' => [
                'copy_link' => 'Copiar link',
                'rename' => 'Renomear',
                'make_copy' => 'Fazer uma cópia',
                'alt_text' => 'Texto ALT',
            ],
            'user' => [
                'favorite' => 'Adicionar aos favoritos',
                'remove_favorite' => 'Remover favorito',
            ],
            'other' => [
                'download' => 'Baixar',
                'trash' => 'Mover para a lixeira',
                'delete' => 'Excluir permanentemente',
                'restore' => 'Restaurar',
                'properties' => 'Propriedades',
            ],
        ],
        'change_image' => 'Alterar imagem',
        'delete_image' => 'Excluir imagem',
        'choose_image' => 'Escolher imagem',
        'preview_image' => 'Visualizar imagem',
    ],
    'name_invalid' => 'O nome da pasta tem caracteres inválidos.',
    'add_from_url' => 'Adicionar da URL',
    'or' => 'ou',
    'url_invalid' => 'Forneça uma URL válida',
    'path_invalid' => 'Forneça uma URL válida path',
    'download_link' => 'Download',
    'url' => 'URL',
    'download_explain' => 'Digite uma URL por linha.',
    'downloading' => 'Baixando...',
    'prepare_file_to_download' => 'Preparando arquivo para download...',
    'update_alt_text_success' => 'Atualização de texto alternativo bem-sucedida!',
    'upload_from_local' => 'Carregar do local',
    'upload_from_url' => 'Carregar da URL',

    'cropper' => [
        'height' => 'Altura',
        'width' => 'Largura',
        'aspect_ratio' => 'Proporção da tela?',
    ],
    'unable_to_write' => 'Não foi possível gravar o arquivo. Por favor, faça chmod na pasta ":folder" para torná-la gravável!',
    'unable_download_image_from' => 'Não foi possível baixar a imagem da URL: :url',
    'rename_physical_folder' => 'Renomeie o nome da pasta física no disco também',
    'rename_physical_file' => 'Renomeie o nome do arquivo físico no disco também',
    'rename_physical_file_warning' => 'Esta opção renomeará o nome do arquivo físico no disco também. Isso pode causar links quebrados se você estiver usando este arquivo em outros lugares.',
    'properties' => [
        'name' => 'Propriedades',
        'color_label' => 'Escolha uma cor para esta pasta',
    ],
    'update_properties_success' => 'Atualize as propriedades com sucesso!',
];
