<?php

namespace Trivium\Media\Chunks\Exceptions;

use Exception;

class ChunkSaveException extends Exception
{
}
