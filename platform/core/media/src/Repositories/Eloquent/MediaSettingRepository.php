<?php

namespace Trivium\Media\Repositories\Eloquent;

use Trivium\Media\Repositories\Interfaces\MediaSettingInterface;
use Trivium\Support\Repositories\Eloquent\RepositoriesAbstract;

class MediaSettingRepository extends RepositoriesAbstract implements MediaSettingInterface
{
}
