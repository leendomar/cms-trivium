<?php

namespace Trivium\Media\Repositories\Interfaces;

use Trivium\Support\Repositories\Interfaces\RepositoryInterface;

interface MediaSettingInterface extends RepositoryInterface
{
}
