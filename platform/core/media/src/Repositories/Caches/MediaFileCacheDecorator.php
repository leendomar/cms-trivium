<?php

namespace Trivium\Media\Repositories\Caches;

use Trivium\Media\Repositories\Eloquent\MediaFileRepository;

/**
 * @deprecated
 */
class MediaFileCacheDecorator extends MediaFileRepository
{
}
