<?php

namespace Trivium\Media\Repositories\Caches;

use Trivium\Media\Repositories\Eloquent\MediaSettingRepository;

/**
 * @deprecated
 */
class MediaSettingCacheDecorator extends MediaSettingRepository
{
}
