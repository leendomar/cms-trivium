<?php

namespace Trivium\Media\Repositories\Caches;

use Trivium\Media\Repositories\Eloquent\MediaFolderRepository;

/**
 * @deprecated
 */
class MediaFolderCacheDecorator extends MediaFolderRepository
{
}
