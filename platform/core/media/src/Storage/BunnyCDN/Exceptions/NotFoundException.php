<?php

namespace Trivium\Media\Storage\BunnyCDN\Exceptions;

class NotFoundException extends BunnyCDNException
{
}
