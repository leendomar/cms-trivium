<?php

namespace Trivium\Media\Storage\BunnyCDN\Exceptions;

class DirectoryNotEmptyException extends BunnyCDNException
{
}
