<?php

use Trivium\Base\Http\Middleware\RequiresJsonRequestMiddleware;
use Trivium\Theme\Facades\Theme;
use Illuminate\Support\Facades\Route;
use Theme\Anna\Http\Controllers\AnnaController;

Theme::registerRoutes(function () {
    Route::group(['controller' => AnnaController::class], function () {
        Route::middleware(RequiresJsonRequestMiddleware::class)
            ->group(function () {
                Route::get('ajax/search', 'getSearch')->name('public.ajax.search');
            });

        // Add your custom route here
        // Ex: Route::get('hello', 'getHello');
    });
});

Theme::routes();
