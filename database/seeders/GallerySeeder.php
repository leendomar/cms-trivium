<?php

namespace Database\Seeders;

use Trivium\Base\Supports\BaseSeeder;
use Trivium\Gallery\Database\Traits\HasGallerySeeder;

class GallerySeeder extends BaseSeeder
{
    use HasGallerySeeder;

    public function run(): void
    {
        $galleries = [
            'Pôr do Sol',
            'Vistas do Oceano',
            'Hora de Aventura',
            'Luzes da Cidade',
            'Paisagem dos Sonhos',
            'Floresta Encantada',
            'Hora Dourada',
            'Serenidade',
            'Beleza Eterna',
            'Magia do Luar',
            'Noite Estrelada',
            'Jóias Escondidas',
            'Águas Tranquilas',
            'Escape Urbano',
            'Zona Crepuscular',
        ];

        $faker = $this->fake();

        $this->createGalleries(
            collect($galleries)->map(function (string $item, int $index) {
                return ['name' => $item, 'is_featured' => $index < 6, 'image' => $this->filePath('news/' . ($index + 6) . '.jpg')];
            })->toArray(),
            array_map(function ($index) use ($faker) {
                return ['img' => $this->filePath('news/' . $index . '.jpg'), 'description' => $faker->text(150)];
            }, range(1, 20))
        );
    }
}
