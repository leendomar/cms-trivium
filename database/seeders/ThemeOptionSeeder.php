<?php

namespace Database\Seeders;

use Trivium\Base\Supports\BaseSeeder;
use Trivium\Page\Models\Page;
use Trivium\Theme\Database\Traits\HasThemeOptionSeeder;

class ThemeOptionSeeder extends BaseSeeder
{
    use HasThemeOptionSeeder;

    public function run(): void
    {
        $this->uploadFiles('general');

        $this->createThemeOptions([
            'site_title' => 'Apenas mais um site do Trivium CMS',
            'seo_description' => 'Com experiência, garantimos que cada projeto seja concluído muito rápido e no prazo com alta qualidade usando nosso Trivium CMS',
            'copyright' => '©%Y Your Company. All rights reserved.',
            'favicon' => $this->filePath('general/favicon.png'),
            'logo' => $this->filePath('general/logo.png'),
            'website' => 'https://trivium.dev.br',
            'contact_email' => 'support@company.com',
            'site_description' => 'Com experiência, garantimos que cada projeto seja concluído muito rápido e no prazo com alta qualidade usando nosso Trivium CMS',
            'phone' => '+(55) 00 9 99999-9999',
            'address' => 'Curitiba',
            'cookie_consent_message' => 'Sua experiência neste site será melhorada ao permitir cookies',
            'cookie_consent_learn_more_url' => 'politica-de-cookies',
            'cookie_consent_learn_more_text' => 'Política de Cookies',
            'homepage_id' => Page::query()->value('id'),
            'blog_page_id' => Page::query()->skip(1)->value('id'),
            'primary_color' => '#AF0F26',
            'primary_font' => 'Roboto',
            'social_links' => [
                [
                    [
                        'key' => 'name',
                        'value' => 'Facebook',
                    ],
                    [
                        'key' => 'icon',
                        'value' => 'ti ti-brand-facebook',
                    ],
                    [
                        'key' => 'url',
                        'value' => 'https://facebook.com',
                    ],
                ],
                [
                    [
                        'key' => 'name',
                        'value' => 'X (Twitter)',
                    ],
                    [
                        'key' => 'icon',
                        'value' => 'ti ti-brand-x',
                    ],
                    [
                        'key' => 'url',
                        'value' => 'https://x.com',
                    ],
                ],
                [
                    [
                        'key' => 'name',
                        'value' => 'YouTube',
                    ],
                    [
                        'key' => 'icon',
                        'value' => 'ti ti-brand-youtube',
                    ],
                    [
                        'key' => 'url',
                        'value' => 'https://youtube.com',
                    ],
                ],
            ],
        ]);
    }
}
