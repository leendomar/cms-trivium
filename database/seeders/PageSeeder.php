<?php

namespace Database\Seeders;

use Trivium\Base\Facades\Html;
use Trivium\Base\Supports\BaseSeeder;
use Trivium\CookieConsent\Database\Traits\HasCookieConsentSeeder;
use Trivium\Page\Database\Traits\HasPageSeeder;

class PageSeeder extends BaseSeeder
{
    use HasPageSeeder;
    use HasCookieConsentSeeder;

    public function run(): void
    {
        $this->truncatePages();

        $pages = [
            [
                'name' => 'Homepage',
                'content' =>
                    Html::tag('div', '[featured-posts][/featured-posts]') .
                    Html::tag('div', '[recent-posts title="O que há de novo?"][/recent-posts]') .
                    Html::tag('div', '[featured-categories-posts title="Talvez você vai gostar..." category_id="2"][/featured-categories-posts]') .
                    Html::tag('div', '[all-galleries limit="8" title="Galerias"][/all-galleries]')
                ,
                'template' => 'no-sidebar',
            ],
            [
                'name' => 'Blog',
                'content' => '---',
            ],
            [
                'name' => 'Contact',
                'content' => Html::tag(
                    'p',
                    'Address: Curitiba, paraná'
                ) .
                    Html::tag('p', 'Email: contato@trivium.dev.br') .
                    Html::tag(
                        'p',
                        '[google-map]Curitiba[/google-map]'
                    ) .
                    Html::tag('p', 'Para uma resposta mais rápida, use o formulário de contato abaixo.') .
                    Html::tag('p', '[contact-form][/contact-form]'),
            ],
            [
                'name' => $this->getCookieConsentPageName(),
                'content' => $this->getCookieConsentPageContent(),
            ],
            [
                'name' => 'Galeria',
                'content' => '<div>[gallery title="Galeria"][/gallery]</div>',
            ],
        ];

        $this->createPages($pages);
    }
}
