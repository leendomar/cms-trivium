<?php

namespace Database\Seeders;

use Trivium\Base\Facades\Html;
use Trivium\Base\Supports\BaseSeeder;
use Trivium\Blog\Database\Traits\HasBlogSeeder;
use Trivium\Media\Facades\RvMedia;

class BlogSeeder extends BaseSeeder
{
    use HasBlogSeeder;

    public function run(): void
    {
        $this->uploadFiles('news');

        $categories = [
            ['name' => 'Inteligência artificial'],
            ['name' => 'Cíber segurança'],
            ['name' => 'Tecnologia Blockchain'],
            ['name' => '5G e Conectividade'],
            ['name' => 'Realidade Aumentada (RA)'],
            ['name' => 'Tecnologia verde'],
            ['name' => 'Computação quântica'],
            ['name' => 'Computação de Borda'],
        ];

        $this->createBlogCategories($categories);

        $tags = [
            ['name' => 'AI'],
            ['name' => 'Aprendizado de máquina'],
            ['name' => 'Redes neurais'],
            ['name' => 'Segurança de dados'],
            ['name' => 'Blockchain'],
            ['name' => 'Criptomoeda'],
            ['name' => 'IoT'],
            ['name' => 'Jogos de RA'],
        ];

        $this->createBlogTags($tags);

        $posts = [
            [
                'name' => 'Avanço na computação quântica: poder de computação atinge marco',
                'description' => 'Pesquisadores alcançam um marco significativo na computação quântica, desbloqueando poder de computação sem precedentes que tem o potencial de revolucionar vários setores.',
            ],
            [
                'name' => 'Implementação do 5G acelera: conectividade de última geração transforma a comunicação',
                'description' => 'A implementação global da tecnologia 5G ganha força, prometendo conectividade mais rápida e confiável, abrindo caminho para inovações em comunicação e IoT.',
            ],
            [
                'name' => 'Gigantes da tecnologia colaboram em estrutura de IA de código aberto',
                'description' => 'Empresas líderes em tecnologia unem forças para desenvolver uma estrutura de inteligência artificial de código aberto, promovendo a colaboração e acelerando os avanços na pesquisa de IA.',
            ],
            [
                'name' => 'SpaceX lança missão para estabelecer a primeira colônia humana em Marte',
                'description' => 'A SpaceX de Elon Musk embarca em uma missão histórica para estabelecer a primeira colônia humana em Marte, marcando um passo significativo em direção à exploração interplanetária.',
            ],
            [
                'name' => 'Avanços na segurança cibernética: novos protocolos reforçam a defesa digital',
                'description' => 'Em resposta às ameaças cibernéticas em evolução, os avanços nos protocolos de segurança cibernética aprimoram as medidas de defesa digital, protegendo indivíduos e organizações de ataques online.',
            ],
            [
                'name' => 'Inteligência artificial na saúde: soluções transformadoras para o atendimento ao paciente',
                'description' => 'As tecnologias de IA continuam a revolucionar a saúde, oferecendo soluções transformadoras para atendimento ao paciente, diagnóstico e planos de tratamento personalizados.',
            ],
            [
                'name' => 'Inovações robóticas: sistemas autônomos remodelam indústrias',
                'description' => 'Sistemas robóticos autônomos redefinem indústrias, pois são cada vez mais adotados para tarefas que vão desde manufatura e logística até saúde e agricultura.',
            ],
            [
                'name' => 'Avanço da realidade virtual: experiências imersivas redefinem o entretenimento',
                'description' => 'Avanços na tecnologia de realidade virtual levam a experiências imersivas que redefinem o entretenimento, jogos e narrativas interativas.',
            ],
            [
                'name' => 'Vestíveis inovadores rastreiam métricas de saúde e melhoram o bem-estar',
                'description' => 'Vestíveis inteligentes com recursos avançados de rastreamento de saúde ganham popularidade, capacitando indivíduos a monitorar e melhorar seu bem-estar por meio de insights de dados personalizados.',
            ],
            [
                'name' => 'Tecnologia para o bem: startups desenvolvem soluções para problemas sociais e ambientais',
                'description' => 'Startups de tecnologia se concentram no desenvolvimento soluções inovadoras para abordar desafios sociais e ambientais, demonstrando o impacto positivo da tecnologia em questões globais.',
            ],
            [
                'name' => 'Assistentes pessoais com tecnologia de IA evoluem: aumentando a produtividade e a conveniência',
                'description' => 'Assistentes pessoais com tecnologia de IA passam por avanços significativos, tornando-se mais intuitivos e capazes de aumentar a produtividade e a conveniência na vida diária dos usuários.',
            ],
            [
                'name' => 'Inovação em blockchain: finanças descentralizadas (DeFi) remodelam o setor financeiro',
                'description' => 'A tecnologia blockchain impulsiona a ascensão das finanças descentralizadas (DeFi), remodelando os sistemas financeiros tradicionais e oferecendo novas possibilidades para transações seguras e transparentes.',
            ],
            [
                'name' => 'Internet quântica: a comunicação segura entra em uma nova era',
                'description' => 'O desenvolvimento de uma internet quântica marca uma nova era na comunicação segura, alavancando o emaranhamento quântico para transmissão de dados virtualmente inquebrável.',
            ],
            [
                'name' => 'Avanços na tecnologia de drones: as aplicações se expandem em todos os setores',
                'description' => 'A tecnologia de drones continua a avançar, expandindo suas aplicações em setores como agricultura, construção, vigilância e serviços de entrega.',
            ],
            [
                'name' => 'Avanço na biotecnologia: CRISPR-Cas9 permite edição genética de precisão',
                'description' => 'A A tecnologia de edição genética CRISPR-Cas9 atinge novos patamares, permitindo modificações precisas e direcionadas no código genético com implicações profundas para a medicina e a biotecnologia.',
            ],
            [
                'name' => 'Realidade aumentada na educação: experiências de aprendizagem interativas para alunos',
                'description' => 'A realidade aumentada transforma a educação, fornecendo aos alunos experiências de aprendizagem interativas e imersivas que aumentam o envolvimento e a compreensão.',
            ],
            [
                'name' => 'IA em veículos autônomos: avanços na tecnologia de carros autônomos',
                'description' => 'Algoritmos e sensores de IA em veículos autônomos continuam a avançar, nos aproximando da adoção generalizada de carros autônomos com recursos de segurança aprimorados.',
            ],
            [
                'name' => 'Inovações em tecnologia verde: soluções sustentáveis ​​para um futuro mais verde',
                'description' => 'As inovações em tecnologia verde se concentram em soluções sustentáveis, que vão de fontes de energia renováveis ​​a práticas de fabricação ecologicamente corretas, contribuindo para um futuro mais verde.',
            ],
            [
                'name' => 'O turismo espacial dispara: empresas comerciais fazem progressos nas viagens espaciais',
                'description' => 'As viagens espaciais comerciais ganham força à medida que empresas privadas fazem avanços significativos na oferta de experiências de turismo espacial, abrindo novas fronteiras para indivíduos aventureiros.',
            ],
            [
                'name' => 'Robôs humanoides na vida cotidiana: companheiros e assistentes de IA',
                'description' => 'Robôs humanoides equipados com inteligência artificial avançada tornam-se mais integrados à vida cotidiana, servindo como companheiros e assistentes em vários cenários.',
            ],
        ];

        $faker = $this->fake();

        foreach ($posts as $index => &$item) {
            $item['content'] =
                ($index % 3 == 0 ? Html::tag(
                    'p',
                    '[youtube-video]https://www.youtube.com/watch?v=Gl3GXJt7NvU[/youtube-video]'
                ) : '') .
                Html::tag('p', $faker->realText(1000)) .
                Html::tag(
                    'p',
                    Html::image(
                        RvMedia::getImageUrl('news/' . $faker->numberBetween(1, 5) . '.jpg', 'medium'),
                        'image',
                        ['style' => 'width: 100%', 'class' => 'image_resized']
                    )
                        ->toHtml(),
                    ['class' => 'text-center']
                ) .
                Html::tag('p', $faker->realText(500)) .
                Html::tag(
                    'p',
                    Html::image(
                        RvMedia::getImageUrl('news/' . $faker->numberBetween(6, 10) . '.jpg', 'medium'),
                        'image',
                        ['style' => 'width: 100%', 'class' => 'image_resized']
                    )
                        ->toHtml(),
                    ['class' => 'text-center']
                ) .
                Html::tag('p', $faker->realText(1000)) .
                Html::tag(
                    'p',
                    Html::image(
                        RvMedia::getImageUrl('news/' . $faker->numberBetween(11, 14) . '.jpg', 'medium'),
                        'image',
                        ['style' => 'width: 100%', 'class' => 'image_resized']
                    )
                        ->toHtml(),
                    ['class' => 'text-center']
                ) .
                Html::tag('p', $faker->realText(1000));
            $item['is_featured'] = $index < 6;
            $item['image'] = $this->filePath('news/' . ($index + 1) . '.jpg');
        }

        $this->createBlogPosts($posts);
    }
}
